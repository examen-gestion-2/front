import React, {
    createContext,
    useContext,
    useEffect,
    useState,
} from "react";
import { ROLS } from "../utils/constants";

const GlobalContext = createContext();

// Global hooks
const useGlobalDrawer = () => {
    const [openDrawer, setOpenDrawer] = useContext(GlobalContext).maindrawer;
    return { openDrawer, setOpenDrawer };
};

const useGlobalUser = () => {
    const [loggedUser, setLoggedUser] = useContext(GlobalContext).loggedUser;
    const {setOpenDrawer} = useGlobalDrawer();

    useEffect(() => {
        localStorage.setItem("user", JSON.stringify(loggedUser));
    }, [loggedUser])
    

    const loginUser = (usr) => {
        
        setLoggedUser({logged: true, rol: usr === 1 ? ROLS.ADMIN : ROLS.KITCHEN})
    }

    const logoutUser = () => {
        setLoggedUser({logged: false});
        setOpenDrawer(false);
    }

    const isLogged = () => {
        return loggedUser.logged;
    }

    const getRol = () => {
        return loggedUser.rol
    }

    return { isLogged, loginUser, logoutUser, getRol };
};

const GlobalProvider = ({ children }) => {
    const maindrawer = useState(false);
    const loggedUser = useState(JSON.parse(localStorage.getItem("user")) || {logged: false});

    const globalContext = {
        maindrawer, loggedUser
    }

    return (
        <GlobalContext.Provider value={globalContext}>
            {children}
        </GlobalContext.Provider>
    );
};

export { GlobalProvider, useGlobalDrawer, useGlobalUser };