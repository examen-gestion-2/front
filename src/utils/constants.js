export const URL = "http://127.0.0.1/api/rest"


export const DRAWER_WIDTH = 240;

export const MODULE_QUERY_NAMES = {
    USERS: {
        ALL: "users-data",
        ONE: "user-data"
    },
    CLIENTS: {
        ALL: "clients-data",
        ONE: "client-data"
    },
    PRODUCTS: {
        ALL: "products-data",
        ONE: "product-data"
    },
    STOCKS: {
        ALL: "stocks-data",
        ONE: "stock-data"
    },
    ORDERS: {
        ALL: "orders-data",
        ONE: "order-data"
    },
    LOGIN: {
        VALIDATE: "login-validate-data"
    }
}

export const ROLS = {
    ADMIN: "ADMIN",
    KITCHEN: "KITCHEN",
}


export const TABLE_OPTIONS = {
    width: "100%",
    striped: true,
    highlightOnHover: "true",
    pointerOnHover: "true",
    fixedHeader: "true",
    responsive: "true",
    lengthChange: "false",
    autoWidth: "false",
    pagination: "true",
    paginationComponentOptions: {
        rowsPerPageText: "Filas por pagina",
        rangeSeparatorText: "de",
        selectAllRowsItem: true,
        selectAllRowsText: "todos"
    }
}


export const PAY_METHODS = ["Efectivo", "Sinpe Móvil"];