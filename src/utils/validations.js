// Validate if an input contains text
export const isValidateRequired = (value) => {
    if (value.toString().trim().length === 0) {
        return "REQUIRED";
    }
};

// Validate if an input not pass the  min length
export const isMinLen = (value, min) => {
    if (value.toString().trim().length < min) {
        return "MINLEN";
    }
};

// Validate if an input not pass the max length
export const isMaxLen = (value, max) => {
    if (value.toString().length > max) {
        return "MAXLEN";
    }
};

// Validate if an input not pass the  min num
export const isMinNum = (value, min) => {
    if (value < min) {
        return "MINNUM";
    }
};

// Validate if an input not pass the max num
export const isMaxNum = (value, max) => {
    if (value > max) {
        return "MAXNUM";
    }
};

// Validate if an input pass an array of validations
export const validateArray = (input, validations) => {
    let value = ''
    for (let validation of validations) {
        value = validation(input);
        if (value) {
            return value;
        }
    }
    return '';
}
