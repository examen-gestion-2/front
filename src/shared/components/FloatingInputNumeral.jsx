import React, { useEffect, useId, useState } from "react";

const FloatingInputNumeral = ({
  updateState, getState,
  inputLabel, inputName,
  min = 0, max = 75000
}) => {
  const [dirty, setDirty] = useState(false);
  const id = useId();

  const handleInput = (e) => {
    let input = e.target.value;

    if (input > max){
      input = max
    }

    updateState(input, inputName)
  };

  useEffect(() => {
    if(getState(inputName).state !== ""){
      setDirty(true)
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getState(inputName).state]);

  return (
    <div className="form-floating mb-3">
      
      <input
        type="number"
        min={min}
        max={max}
        className={`form-control ${dirty && getState(inputName).message ? "is-invalid" : ""}`}
        id={id}
        placeholder=" "
        value={getState(inputName).state}
        onChange={handleInput}
        autoComplete="on"
        onBlur={(e) => !e.target.value ? updateState(0, inputName) : updateState(e.target.value, inputName) }
      />

      <label className="floatingInput" htmlFor={id}>
        {inputLabel}
      </label>
      <p className="input-error">{dirty && getState(inputName).message} </p>
    </div>
  );
};

export default FloatingInputNumeral;