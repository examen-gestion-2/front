import React from "react";

const Button = ({ text, handler, color, type }) => {
  return (
    <button type={type} onClick={handler} className={`btn py-2 px-4  btn-${color}`} >
      {text}
    </button>
  );
};

export default Button;
