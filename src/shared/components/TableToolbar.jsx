import React from "react";

const TableToolbar = ({handleAdd, searchValue, handleFilterChange, search}) => {
  return (
    <div className="tool-bar">
      <button
        onClick={handleAdd}
        className="btn btn-primary btn-sm btn-add"
      >
        <i className="ico i__add" /> Agregar
      </button>

      <div className="searchBar">
        <input
          type="text"
          placeholder="Buscar"
          className="textField"
          name="busqueda"
          value={searchValue}
          onChange={handleFilterChange}
        />
        <button type="button" className="btnBuscar" onClick={search}>
          <i className="ico i__search" />
        </button>
      </div>
    </div>
  );
};

export default TableToolbar;
