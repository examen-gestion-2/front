import { useState, useEffect } from "react";
import { URL } from "../../utils/constants";
import CustomLoader from "./CustomLoader";

import placeholder from "./image_placeholder.jpg";

const Image = ({ id }) => {
  const [img, setImg] = useState("");
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true)
    fetch(`${URL}/product/image/${id}`)
      .then((data) => data.text())
      .then((data) => { setImg(data); setLoading(false) })
      .catch(() => setLoading(false));
  }, [id]);

  return <>
        {
            loading ? <CustomLoader/> :  <img className="modal__product-image" src={img.includes("image") ? img : placeholder} alt="Imagen"/>
        }
  </>
;
};

export default Image;
