import React from 'react'

import './customLoader.scss'

const CustomLoader = () => {
  return (
    <span className="loader"></span>
  )
}

export default CustomLoader