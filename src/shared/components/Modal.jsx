
import "./modal.scss";

const Modal = ({ children, handleCancel, visible }) => {

  return (
    visible && (
      <div className="modal__container">
        <div className="modal__card">
          <button className="modal__close-btn" onClick={handleCancel}>
            ❌
          </button>

          {children}
        </div>
      </div>
    )
  );
};

export default Modal;
