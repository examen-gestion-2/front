import React, { useEffect, useId, useState } from "react";

const Textarea = ({
  updateState, getState,
  inputLabel, inputType, inputName,
}) => {
  const [dirty, setDirty] = useState(false);
  const id = useId();

  const handleInput = (e) => updateState(e.target.value, inputName);

  useEffect(() => {
    if(getState(inputName).state !== ""){
      setDirty(true)
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getState(inputName).state]);

  return (
    <div className="form-floating mb-4">
      <textarea
        
        type={inputType}
        className={`text-area form-control ${dirty && getState(inputName).message ? "is-invalid" : ""}`}
        id={id}
        placeholder=" "
        value={getState(inputName).state}
        onChange={handleInput}
        autoComplete="on"
      />

      <label className="floatingInput" htmlFor={id}>
        {inputLabel}
      </label>
      <p className="input-error">{dirty && getState(inputName).message} </p>
    </div>
  );
};

export default Textarea;