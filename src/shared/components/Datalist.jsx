import { useEffect, useId, useState } from "react";

const Datalist = ({
  items,
  updateState, getState,
  inputLabel,
  inputName,
  overridesOnBlur,
}) => {
  const [dirty, setDirty] = useState(false);

  const id = useId();

  const handleInput = (e) => updateState(e.target.value, inputName);

  const handleBlur = (e) => {
    if (!items.includes(e.target.value)) {
      updateState("", inputName);
    }
  };

  useEffect(() => {
    if (getState(inputName).state !== "") {
      setDirty(true);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getState(inputName).state]);

  return (
    <div className="form-floating mb-3">
      <input
        value={getState(inputName).state}
        onChange={handleInput}
        onBlur={overridesOnBlur ? overridesOnBlur : handleBlur}
        type="text"
        className={`form-control ${
          dirty && getState(inputName).message ? "is-invalid" : ""
        }`}
        list={`${id}datalistOptions`}
        id="id"
        placeholder=" "
      />
      <label type="text" className="floatingInput" htmlFor="id">
        {inputLabel}
      </label>
      <p className="input-error">{dirty && getState(inputName).message} </p>
      <datalist id={`${id}datalistOptions`}>
        {items.map((item, i) => (
          <option key={i}>{item}</option>
        ))}
      </datalist>
      
    </div>
  );
};

export default Datalist;
