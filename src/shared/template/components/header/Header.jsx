import "./header.scss";

import AppMenuBar from "./components/MenuAppBar";

const Header = () => {
  return (
    <header className="header">
          <AppMenuBar></AppMenuBar>
    </header>
  );
};

export default Header;