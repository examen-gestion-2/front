import React from "react";

import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";

import { useGlobalDrawer } from '../../../../../contexts/globalContext';

const OpenDrawerBtn = () => {

  const {openDrawer, setOpenDrawer} = useGlobalDrawer()

  return (
    <IconButton
      color="inherit"
      aria-label="open drawer"
      onClick={() => {
        setOpenDrawer(true);
      }}
      edge="start"
      sx={{
        marginRight: 5,
        ...(openDrawer && { display: "none" }),
      }}
    >
      <MenuIcon />
    </IconButton>
  );
};

export default OpenDrawerBtn;