import { useState } from 'react';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { Link, useNavigate } from "react-router-dom";

import OpenDrawerBtn from './OpenDrawerBtn';

import { AppBarStyle } from './ComponentStyles';
import { useGlobalDrawer, useGlobalUser } from '../../../../../contexts/globalContext';
import icon from "../../../../../assets/images/sushi_asset.png"


const AppBar = AppBarStyle;

export default function MenuAppBar() {
  const {isLogged} = useGlobalUser()

  const navigate = useNavigate()

  const {openDrawer, setOpenDrawer} = useGlobalDrawer()

  const [timer, setTimer] = useState(0);

  const handleDown = () => {
    setTimer(new Date());
  }

  const handleUp = () => {
    const actualDate = new Date();
    const diference = actualDate - timer;
    
    if (diference > 200){
      navigate("/login");
    }
  }

  return (
    <AppBar position="fixed" open={openDrawer}>
    <Toolbar>
      { isLogged() && 
        <OpenDrawerBtn setOpenDrawer={setOpenDrawer} openDrawer={openDrawer}></OpenDrawerBtn>
      }
      
      <Link to={"/"}>
        <Typography variant="h6" noWrap component="div">
          Happy Sushi
        </Typography>
      </Link>

      { !isLogged() && 
      <img className='admin_icon' src={icon} alt="alter_icon" onMouseDown={handleDown} onMouseUp={handleUp}></img>
      }
    </Toolbar>
  </AppBar>
  );
}