import "./footer.scss";

import WhatsAppIcon from '@mui/icons-material/WhatsApp';
import InstagramIcon from '@mui/icons-material/Instagram';
import FacebookIcon from '@mui/icons-material/Facebook';

const Footer = () => {
  
  const links = [
    { "link": "/", "text": "Whatsapp", "icon":<WhatsAppIcon/> },
    { "link": "/login", "text": "Instagram", "icon":<InstagramIcon/>  },
    { "link": "/icons", "text": "Facebook", "icon":<FacebookIcon/>  }
  ]
  

  return (
    <header className="footer">
      <nav className="footer-nav">
        {links.map(({ link, text, icon }) => 
          <a key={text} className="footer-nav__link" href={link} title={text}> {icon} </a>
          
        )}
      </nav>
    </header>
  );
};

export default Footer;
