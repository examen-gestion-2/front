import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";

import List from "@mui/material/List";
import CssBaseline from "@mui/material/CssBaseline";
import Divider from "@mui/material/Divider";

import IconButton from "@mui/material/IconButton";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import LogoutIcon from "@mui/icons-material/Logout";

import { DrawerHeaderStyle, DrawerStyle } from "./ComponentStyles";
import MenuDrawerBtn from "./MenuDrawerBtn";
import useMainDrawer from "./useMainDrawer";
import {
  useGlobalDrawer,
  useGlobalUser,
} from "../../../../../contexts/globalContext";
import MenuListDrawerBtn from "./MenuListDrawerBtn";
import { Fragment } from "react";
import { ROLS } from "../../../../../utils/constants";
const DrawerHeader = styled("div")(DrawerHeaderStyle);
const Drawer = DrawerStyle();

export default function MainDrawer({ children }) {
  const { openDrawer, setOpenDrawer } = useGlobalDrawer();

  const { isLogged, logoutUser, getRol } = useGlobalUser();

  const {
    adminUpperLinks,
    adminMiddleLinks,
    adminLowerLinks,
    kitchenUpperLinks,
    kitchenmiddleLinks,
    handleDrawerClose,
    theme,
  } = useMainDrawer(setOpenDrawer);

  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />
      {isLogged() && (
        <>
          <Drawer variant="permanent" open={openDrawer}>
            <DrawerHeader>
              <IconButton onClick={handleDrawerClose}>
                {theme.direction === "rtl" ? (
                  <ChevronRightIcon />
                ) : (
                  <ChevronLeftIcon />
                )}
              </IconButton>
            </DrawerHeader>
            <Divider />
            <List>
              {getRol() === ROLS.ADMIN && (
                <Fragment>
                  <MenuListDrawerBtn links={adminUpperLinks}></MenuListDrawerBtn>
                  <Divider />
                  <MenuListDrawerBtn links={adminMiddleLinks}></MenuListDrawerBtn>
                  <Divider />
                  <MenuListDrawerBtn links={adminLowerLinks}></MenuListDrawerBtn>
                 
                </Fragment>
              )}
              {getRol() === ROLS.KITCHEN && (
                <Fragment>
                <MenuListDrawerBtn links={kitchenUpperLinks}></MenuListDrawerBtn>
                <Divider />
                <MenuListDrawerBtn links={kitchenmiddleLinks}></MenuListDrawerBtn>
                </Fragment>
              )}
              <Divider />
              <MenuDrawerBtn
                openDrawer={openDrawer}
                icon={<LogoutIcon />}
                link={"/"}
                text={"Cerrar sesión"}
                handleClick={() => {
                  setOpenDrawer(false);
                  logoutUser();
                }}
              />
            </List>
          </Drawer>
          <>dfgsdfgsdrg</>
        </>
      )}
      <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
        <DrawerHeader />
        {children}
      </Box>
    </Box>
  );
}