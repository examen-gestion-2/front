import React from 'react'
import WhatsAppIcon from '@mui/icons-material/WhatsApp';
import PhoneInTalkIcon from '@mui/icons-material/PhoneInTalk';

const FloatingButtons = () => {
  return <>
            <div className="fixedcall">
                <a className="phonenumber" href="tel:+50689839080">
                        <span><PhoneInTalkIcon/></span>
                        <p>Ordena ya <br/>8983-9080</p>
                    </a>
                </div>
                <div className="fixedwhatapp">
                <a className="phonenumber" href="https://wa.me/50689839080"><span> 
                        <WhatsAppIcon/> </span>
                        <p>WhatsApp <br/>8983-9080</p>
                    </a>
                </div>
         </>

}

export default FloatingButtons