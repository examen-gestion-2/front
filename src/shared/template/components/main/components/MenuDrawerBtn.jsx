import React from "react";

import { Link } from "react-router-dom";

import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import { useGlobalDrawer } from "../../../../../contexts/globalContext";

const MenuDrawerBtn = ({ link, text, icon, handleClick = ()=>{} }) => {

  const { openDrawer } = useGlobalDrawer()

  return (
   
        <Link to={link} onClick={handleClick}>
          <ListItem key={text} disablePadding sx={{ display: "block" }}>
            <ListItemButton
              sx={{
                minHeight: 48,
                justifyContent: openDrawer ? "initial" : "center",
                px: 2.5,
              }}
            >
              <ListItemIcon
                sx={{
                  minWidth: 0,
                  mr: openDrawer ? 3 : "auto",
                  justifyContent: "center",
                }}
              >
                {icon}
              </ListItemIcon>
              <ListItemText
                primary={text}
                sx={{ opacity: openDrawer ? 1 : 0 }}
              />
            </ListItemButton>
          </ListItem>
        </Link>
  );
};

export default MenuDrawerBtn;
