import { useTheme } from "@mui/material/styles";

import {
  BILLING_DL, CLIENTS_DL, INDEX_DL, KITCHEN_DL, ORDERS_DL,
  PRODUCTS_DL, REPORTS_DL, STOCK_DL, USERS_DL, MENU_DL
} from "./DrawerLinks";

const useMainDrawer = (setOpenDrawer) => {

  const theme = useTheme();

  const handleDrawerClose = () => {
    setOpenDrawer(false);
  };

  const adminUpperLinks = [INDEX_DL, USERS_DL];
  const adminMiddleLinks = [PRODUCTS_DL, ORDERS_DL, CLIENTS_DL, BILLING_DL];
  const adminLowerLinks = [REPORTS_DL, STOCK_DL];
  const kitchenUpperLinks = [MENU_DL];
  const kitchenmiddleLinks = [KITCHEN_DL];

  return { adminUpperLinks, adminMiddleLinks, adminLowerLinks, kitchenUpperLinks, kitchenmiddleLinks, theme, handleDrawerClose }
}

export default useMainDrawer