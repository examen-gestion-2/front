import { useGlobalDrawer } from "../../../../../contexts/globalContext";
import MenuDrawerBtn from "./MenuDrawerBtn";

const MenuListDrawerBtn = ({ links }) => {
  const { openDrawer } = useGlobalDrawer();

  return (
    <>
      {links.map((link, i) => (
        <MenuDrawerBtn
          key={i}
          openDrawer={openDrawer}
          {...link}
        ></MenuDrawerBtn>
      ))}
    </>
  );
};

export default MenuListDrawerBtn;
