import React from 'react';

import HomeIcon from "@mui/icons-material/Home";
import PersonIcon from "@mui/icons-material/Person";
import DescriptionIcon from '@mui/icons-material/Description';
import GroupIcon from '@mui/icons-material/Group';
import RestaurantIcon from '@mui/icons-material/Restaurant';
import GradingIcon from '@mui/icons-material/Grading';
import SummarizeIcon from '@mui/icons-material/Summarize';
import InventoryIcon from '@mui/icons-material/Inventory';

// Drawer links with icons
export const INDEX_DL = { text: "Inicio", icon: <HomeIcon />, link: "/" };
export const USERS_DL = { text: "Usuarios", icon: <PersonIcon />, link: "/users" };
export const PRODUCTS_DL = { text: "Productos", icon: <RestaurantIcon />, link: "/products" };
export const MENU_DL = { text: "Menú", icon: <RestaurantIcon />, link: "/menu" };
export const ORDERS_DL = { text: "Pedidos", icon: <GradingIcon />, link: "/orders" };
export const KITCHEN_DL = { text: "Cocina", icon: <GradingIcon />, link: "/kitchen" };
export const CLIENTS_DL = { text: "Clientes", icon: <GroupIcon />, link: "/clients" }
export const BILLING_DL = { text: "Facturacion", icon: <DescriptionIcon />, link: "/billing" }
export const REPORTS_DL = { text: "Reportes", icon: <SummarizeIcon />, link: "/reports" }
export const STOCK_DL = { text: "Inventario", icon: <InventoryIcon />, link: "/stock" }