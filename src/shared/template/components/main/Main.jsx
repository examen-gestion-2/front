import "./main.scss";
import MainDrawer from "./components/MainDrawer";
import FloatingButtons from "./components/FloatingButtons";

const Main = ({ children }) => {
  return (
    <main className="main">
      <MainDrawer children={children} />
      <FloatingButtons />
    </main>
  );
};

export default Main;
