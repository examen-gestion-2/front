import { Link } from "react-router-dom";
import "./error404.scss";

const Error404 = () => {
  return (
    <section>
      <p>¿Cómo has llegado hasta aquí?</p>
      <h2>404</h2>
      <p>
        Sitio <b>no encontrado</b>
      </p>
      <Link className="page404-link" to="/">Ir al inicio</Link>
    </section>
  );
};

export default Error404;
