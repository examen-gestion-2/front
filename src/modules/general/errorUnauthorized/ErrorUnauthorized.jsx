import { Link } from "react-router-dom";

const ErrorUnauthorized = () => {
  return (
    <section>
      <p>No tienes permiso para acceder aqui</p>
      <Link to="/">Ir al inicio</Link>
    </section>
  );
};

export default ErrorUnauthorized;
