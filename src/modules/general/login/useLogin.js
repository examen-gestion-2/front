/* eslint-disable react-hooks/exhaustive-deps */

import { useEffect } from "react";
import { useGlobalUser } from "../../../contexts/globalContext";

import { useNavigate } from "react-router-dom";

import useAlerts from "../../../hooks/useAlerts";
import useValidateLoginData from "./useValidateLoginData";
import { pwdErrors, usrErrors, validatePassword, validateUsername } from "./loginValidator";
import useFormHandler from "../../../hooks/useFormHandler";

const useLogin = () => {
    const navigate = useNavigate();

    const { toastAlert } = useAlerts();
    const { isLogged, loginUser } = useGlobalUser();
    const formHandler = useFormHandler({ userName: "", password: "" })
    const { state } = formHandler;
    const userName = formHandler.getState('userName').state;
    const password = formHandler.getState('password').state;

    const validateUser = useValidateLoginData({ userName, password });

    useEffect(() => {
        const code = validateUsername(userName)
        const message = usrErrors[code]
        formHandler.updateMessage(message, 'userName')
    }, [state.userName.state])

    useEffect(() => {
        const code = validatePassword(password)
        const message = pwdErrors[code]
        formHandler.updateMessage(message, 'password')
    }, [state.password.state])


    const handleSubmit = (e) => {
        e.preventDefault();

        if (validatePassword(password) !== '' | validateUsername(userName) !== '') {
            toastAlert("Datos no válidos.", "error");
            return;
        }

        validateUser.refetch();
    };

    useEffect(() => {
        if (isLogged()) {
            navigate("/");
        }
    }, [isLogged, navigate]);

    useEffect(() => {
        if (!validateUser.data) {
            return;
        }

        if (validateUser.data.status !== "success") {
            toastAlert("Inicio de sesión inválido.", "error");
            return;
        }

        loginUser(validateUser.data.rol);
        navigate("/");
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [validateUser]);

    const inputData = [
        { inputLabel: "Usuario", inputType: "Text", updateState: formHandler.updateState, getState: formHandler.getState, inputName: 'userName', state: state },
        { inputLabel: "Contraseña", inputType: "password", updateState: formHandler.updateState, getState: formHandler.getState, inputName: 'password', state: state }
    ]

    return {
        handleSubmit, userName, password, updateState: formHandler.updateState, getState: formHandler.getState, inputData
    }
}

export default useLogin