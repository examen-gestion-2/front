import { isValidateRequired, isMinLen, isMaxLen, validateArray } from "../../../utils/validations";

const userValidations = [(input) => isValidateRequired(input), (input) => isMinLen(input, 5), (input) => isMaxLen(input, 25)];
const passValidations = [(input) => isValidateRequired(input), (input) => isMinLen(input, 5), (input) => isMaxLen(input, 75)];

export const pwdErrors = {
    REQUIRED: "La contraseña es requerida",
    MINLEN: "Se requieren al menos 5 carácteres",
    MAXLEN: "El máximo es de 25 carácteres"
}

export const usrErrors = {
    REQUIRED: "El nombre de usuario es requerido",
    MINLEN: "Se requieren al menos 5 carácteres",
    MAXLEN: "El máximo es de 25 carácteres"
}

export const validatePassword = (input) => {
    return validateArray(input, passValidations)
}

export const validateUsername = (input) => {
    return validateArray(input, userValidations)
}