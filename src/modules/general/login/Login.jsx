import { Fragment } from "react";
import FloatingInput from "../../../shared/components/FloatingInput";

import "./login.scss";
import useLogin from "./useLogin";

const Login = () => {
  const {
    handleSubmit,
    inputData
  } = useLogin();

  return (
    <Fragment>
      <div className="login-form__container ">
        <form onSubmit={handleSubmit}>
          <h2 className="login-form__title">Acceso administrativo</h2>

          <div className="row">

            { inputData.map(data => <FloatingInput key={data.inputLabel} {...data} />) }

            <div className="d-grid gap-2 mt-1">
              <button className="btn btn-primary p-2" type="submit" name="btn">
                Login
              </button>
            </div>
          </div>
        </form>
      </div>
    </Fragment>
  );
};

export default Login;