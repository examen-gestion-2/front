import { useQuery } from "react-query";
import { MODULE_QUERY_NAMES } from "../../../utils/constants";
import useLoginService from "./loginService";

const useValidateLoginData = ({userName, password}) => {
    const { validateCredentials } = useLoginService();

    const queryOptions = {     
        enabled: false,
        cacheTime: 0
    };

    const { isLoading, isFetching, error, data, refetch } =  useQuery(
        [MODULE_QUERY_NAMES.LOGIN.VALIDATE,{userName, password}], 
        () => validateCredentials({userName, password}), 
        queryOptions
    ); 

    return { isLoading, isFetching, error, data, refetch }
}

export default useValidateLoginData