import useFetch from "../../../hooks/useFetch"
import { URL } from "../../../utils/constants";

const serviceUrl = `${URL}/user`

const useLoginService = () => {
    const { fetchData } = useFetch();

    // Data contains an username and password
    const validateCredentials = (data) => {
        const options = { method: 'POST', body: JSON.stringify(data) }
        return fetchData({ url: `${serviceUrl}/login`, options });
    }

    return { validateCredentials };
}

export default useLoginService;