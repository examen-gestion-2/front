import "./icons.scss";
import icons from "./icons_data.json";

import useAlerts from "../../../hooks/useAlerts";

/*
 * Dev page to manage actual items.
 */

const IconsPage = () => {
  const { toastAlert } = useAlerts();

  const copyIcon = (icon) => {
    if ("clipboard" in navigator) {
      const iconData = `<i className="ico i__${icon}"/>`;
      navigator.clipboard.writeText(iconData);
      toastAlert("Copiado al portapapeles");
    }
  };

  return (
    <div className="icons-container">
      {icons.map((icon) => (
        <div key={icon} className="icon-card" onClick={() => copyIcon(icon)}>
          <i className={`ico i__${icon}`}></i>
          <span> {`i__${icon}`} </span>
        </div>
      ))}
    </div>
  );
};

export default IconsPage;
