import { useQuery } from "react-query";
import { MODULE_QUERY_NAMES } from "../../utils/constants";
import ClientService from "./clientsService";

// Obtains an unique client to edit.
const useClientData = (id) => {

    const { getClient } = ClientService();

    const queryOptions = {
        enabled: false,
        cacheTime: 0,
    };

    const { isLoading, isFetching, error, data, refetch } = useQuery(
        [MODULE_QUERY_NAMES.CLIENTS.ONE, { id }],
        () => { if (id) return getClient(id) },
        {
            refetchIntervalInBackground: false,
            refetchOnWindowFocus: false,
            ...queryOptions
        }
    );

    return { isLoading, isFetching, error, data, refetch }
}

export default useClientData