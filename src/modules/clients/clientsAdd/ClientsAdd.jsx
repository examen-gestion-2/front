import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router';
import ClientsModal from '../clientsModal/ClientsModal'

import { newFormValues } from "../clientsModal/useClientsModal"; 
import useClientData from '../useClientData';

const ClientsAdd = () => {

  const { id } = useParams();
  const [visible, setVisible] = useState(true);
  const [editClient, setEditClient] = useState(newFormValues);

  const validateClient = useClientData(id);

  useEffect(() => {
    validateClient.refetch();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  useEffect(() => {
    if (validateClient.data && !validateClient.data.error) {
      const clientData = {
        id: validateClient.data.clientId,
        name: validateClient.data.name,
        phone: validateClient.data.phone,
        address: validateClient.data.address,
        registerDate: validateClient.data.registerDate, 
      };

      setEditClient(clientData)
    }
  }, [validateClient.data]);

  return (
    <>
        <ClientsModal
          productFetch = {id ? validateClient : null}
          key={new Date().getTime()}
          visible={visible}
          setVisible={setVisible}
          editValues={editClient}
      ></ClientsModal>
    </>
  )
}

export default ClientsAdd