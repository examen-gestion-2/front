import { isValidateRequired, isMinLen, isMaxLen, validateArray, isMinNum, isMaxNum } from "../../utils/validations";

const nameValidations = [isValidateRequired, (input) => isMinLen(input, 3), (input) => isMaxLen(input, 25)];
const phoneValidations = [isValidateRequired, (input) => isMinLen(input, 8), (input) => isMaxLen(input, 8)];
const addressValidations = [isValidateRequired, (input) => isMinLen(input, 5), (input) => isMaxLen(input, 75)];
const registerDateValidations = [isValidateRequired, (input) => isMinNum(input, 0), (input) => isMaxNum(input, 75000)];

export const nameErrors = {
    REQUIRED: "El nombre es requerido",
    MINLEN: "Se requieren al menos 3 carácteres",
    MAXLEN: "El máximo es de 25 carácteres"
}

export const validateName = (input) => {
    return validateArray(input, nameValidations)
}

export const phoneErrors = {
    REQUIRED: "El Teléfono es requerido",
    MINLEN: "Se requieren al menos 8 carácteres",
    MAXLEN: "El máximo es de 8 carácteres"
}

export const validatePhone = (input) => {
    return validateArray(input, phoneValidations)
}

export const addressErrors = {
    REQUIRED: "La dirección es requerida",
    MINLEN: "Se requieren al menos 5 carácteres",
    MAXLEN: "El máximo es de 25 carácteres"
}

export const validateAddress = (input) => {
    return validateArray(input, addressValidations)
}

export const registerDateErrors = {
    MINNUM: "La fecha de registro no puede ser menor a 0",
    MAXNUM: "La fecha de registro no puede ser superior a 75.000"
}

export const validateRegisterDate = (input) => {
    return validateArray(input, registerDateValidations)
}
