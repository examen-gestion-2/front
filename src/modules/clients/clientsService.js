import useFetch from "../../hooks/useFetch"

import { URL } from "../../utils/constants";

const serviceUrl = `${URL}/client`

const ClientService = () => {
    const { fetchData } = useFetch();

    const getClients = () => {
        return fetchData({ url: `${serviceUrl}` })
    }

    const getClient = (id) => {
        return fetchData({ url: `${serviceUrl}/${id}` })
    }

    const addClient = (data) => {
        const options = { method: 'POST', body: JSON.stringify(data) }
        return fetchData({ url: `${serviceUrl}`, options });
    }

    const updateClient = (data) => {
        const { id } = data;
        const options = { method: 'PUT', body: JSON.stringify(data) }
        return fetchData({ url: `${serviceUrl}/${id}`, options });
    }

    const deleteClient = (id) => {
        const options = { method: 'DELETE' }
        return fetchData({ url: `${serviceUrl}/${id}`, options });
    }

    return { getClients, getClient, addClient, updateClient, deleteClient };

}

export default ClientService;