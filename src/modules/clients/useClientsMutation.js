import { useNavigate } from "react-router-dom";

import useMutations from "../../hooks/useMutations";
import useAlerts from "../../hooks/useAlerts";
import ClientService from "./clientsService";
import { MODULE_QUERY_NAMES } from "../../utils/constants";

const useClientsMutations = () => {
    const { addClient, deleteClient, updateClient } = ClientService();
    const {toastAlert} = useAlerts();
    const navigate = useNavigate();

    const onSuccessDelete = () => toastAlert("Se eliminó exitosamente", "success");
    const onSuccessUpdate = () => {
        toastAlert("Se actualizó exitosamente", "success");
        navigate(-1);
    }
    const onSuccessAdd = () => {
        toastAlert("Se agregó exitosamente", "success");
        navigate(-1);
    };
    const onErrorDelete = () => toastAlert("No se pudo eliminar", "error");
    const onErrorUpdate = () => toastAlert("No se pudo actualizar", "error");
    const onErrorAdd = () => toastAlert("No se pudo agregar", "error");

    // Base mutations
    const { handleAdd, handleDelete, handleUpdate } = useMutations(
        {
            moduleName:MODULE_QUERY_NAMES.CLIENTS.ALL, 
            addFunction:addClient, 
            deleteFunction:deleteClient, 
            updateFunction:updateClient,
            onSuccessUpdate,
            onErrorUpdate,
            onSuccessDelete,
            onSuccessAdd,
            onErrorDelete,
            onErrorAdd
        });

    return { handleAdd, handleDelete, handleUpdate }
}

export default useClientsMutations