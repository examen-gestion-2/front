/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect} from "react";
import useAlerts from "../../../hooks/useAlerts";

import {
  nameErrors,
  phoneErrors,
  addressErrors,
  registerDateErrors,
  validateName,
  validatePhone,
  validateAddress,
  validateRegisterDate,
} from "../clientsValidator";
import useClientsMutations from "../useClientsMutation";
import { useNavigate } from "react-router";
import useFormHandler from "../../../hooks/useFormHandler";

export const newFormValues = {
  id: 0,  name: "", phone: "", 
   address: "", registerDate: "",
};

export const newValidValues = {
  name: true, phone: true, 
  address: true, registerDate: true, 
};

const useClientsModal = (editValues = newFormValues) => {

  const formHandler = useFormHandler(editValues ? {...editValues} : newFormValues) ///Acá
  const { state } = formHandler;

  const name = formHandler.getState('name').state;
  const phone = formHandler.getState('phone').state;
  const address = formHandler.getState('address').state;
  const registerDate = formHandler.getState('registerDate').state;
  
  const nav = useNavigate();
  const { toastAlert, promiseAlert } = useAlerts();
  const { handleAdd, handleUpdate } = useClientsMutations();

  const inputData = {
    name: {
      inputLabel: "Nombre",
      inputType: "Text",
      inputName: "name",
      updateState: formHandler.updateState,
      getState: formHandler.getState,

    },

    phone: {
      inputLabel: "Teléfono",
      inputType: "text",
      inputName: "phone",
      updateState: formHandler.updateState,
      getState: formHandler.getState,
    },

    address: {
      inputLabel: "Dirección",
      inputType: "Text",
      inputName: "address",
      updateState: formHandler.updateState,
      getState: formHandler.getState,

    },

    registerDate: {
      inputLabel: "Fecha de registro",
      inputType: "Date",
      inputName: "registerDate",
      updateState: formHandler.updateState,
      getState: formHandler.getState,
    },
   
  };

  useEffect(() => {
    const code = validateName(name)
    const message = nameErrors[code]
    formHandler.updateMessage(message, 'name')
  }, [state.name.state])

  useEffect(() => {
    const code = validatePhone(phone)
    const message = phoneErrors[code]
    formHandler.updateMessage(message, 'phone')
  }, [state.phone.state])

  useEffect(() => {
    const code = validateAddress(address)
    const message = addressErrors[code]
    formHandler.updateMessage(message, 'address')
  }, [state.address.state])

  useEffect(() => {
    const code = validateRegisterDate(registerDateErrors)
    const message = registerDateErrors[code]
    formHandler.updateMessage(message, 'registerDate')
  }, [state.registerDate.state])

  const handleSubmit = (e) => {
    e.preventDefault();

    if (
      validateName(name) ||
      validatePhone(phone) ||
      validateAddress(address) ||
      validateRegisterDate(registerDate) 
    ) {
      toastAlert("Datos no válidos.", "error");
      return;
    }

    if (formHandler.getState("id").state > 0) {
      handleUpdate({ ...formHandler.getJson(), registerDate: formHandler.getState('registerDate').state, productId: formHandler.getState("id").id });
    } else {
      handleAdd({ ...formHandler.getJson(), registerDate: formHandler.getState('registerDate').state})
    }
  };

  const handleCancel = () => {
    if (
      JSON.stringify(formHandler.getJson()) === JSON.stringify({newFormValues}) ||
      JSON.stringify(formHandler.getJson()) === JSON.stringify({...editValues})
    ) {
      nav(-1);
      return;
    }

    promiseAlert(
      "Atención",
      `Se perderán los datos ${formHandler.getState("id").id > 0 ? "modificados" : "guardados"
      } ¿Desea continuar?`
    ).then((data) => { 
      if (data.isConfirmed) {
        nav(-1);
        return;
      }
    });
  };

  return { inputData, handleSubmit, handleCancel, formHandler}
}

export default useClientsModal