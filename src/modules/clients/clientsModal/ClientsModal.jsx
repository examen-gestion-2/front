import "./ClientsModal.scss";

import FloatingInput from "../../../shared/components/FloatingInput";
import Textarea from "../../../shared/components/Textarea";

import useClientsModal, { newFormValues } from "./useClientsModal";
import CustomLoader from "../../../shared/components/CustomLoader";
import Modal from "../../../shared/components/Modal";
import Button from "../../../shared/components/Button";

const ClientsModal = ({ clientFetch = null, editValues = newFormValues }) => {
  const { 
    inputData,
    handleSubmit,
    handleCancel,
    formHandler
  } = useClientsModal(editValues);

  const buttonsData = {
    cancel: {color:"danger",type:"button", text:"Cancelar", handler:handleCancel},
    submit: {color:"primary",type:"submit", text: formHandler.getState("id").state > 0 ? "Modificar" : "Registrar"}
  }
    
  return (
    <Modal handleCancel={handleCancel} visible={true}>
      {clientFetch && !clientFetch.data ? (
        <CustomLoader></CustomLoader>
      ) : (
        
        <form onSubmit={handleSubmit}>
          <h4 className="login-form__title mt-3">
            {formHandler.getState("id").state > 0 ? "Modificar" : "Agregar"} cliente
          </h4>
          <div className="row">
              <div className="col">
                  <FloatingInput {...inputData.name} />
                  <FloatingInput {...inputData.phone}/>
                  <FloatingInput {...inputData.registerDate}/>
             </div>
             <div className="col">
                  <div>
                     <Textarea {...inputData.address}></Textarea>
                  </div>
                  <div className="modal__action-container">
                    <Button {...buttonsData.cancel}/>
                    <Button {...buttonsData.submit}/>  
                  </div>

              </div>
             
          </div>

          
        </form>
      
      )}
    </Modal>
  );
};

export default ClientsModal;