/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState} from "react";

import DataTable from "react-data-table-component";
import CustomLoader from "../../../shared/components/CustomLoader";
import WhatsAppIcon from '@mui/icons-material/WhatsApp';
import useClientsData from "../useClientsData";
import useClientsMutations from "../useClientsMutation";
import useClientTable from "./useClientTable";
import useAlerts from "../../../hooks/useAlerts";
import { useNavigate } from "react-router";
import TableToolbar from "../../../shared/components/TableToolbar";
import { TABLE_OPTIONS } from "../../../utils/constants";


const ClientsTable = () => {

  const { handleDelete } = useClientsMutations();
  const { isLoading, data } = useClientsData();

  const { promiseAlert } = useAlerts();
  const { formatDate } = useClientTable();

  const nav = useNavigate();

  const [filteredData, setFilteredData] = useState([]);
  const [searchValue, setSearchValue] = useState('');

  useEffect(() => {
    setFilteredData(data);
  }, [data]);

  const columns = [
    {
      name: "Nombre",
      selector: (row) => row.name,
    },
    {
      name: "Teléfono",
      selector: (row) => row.phone,
    },
    {
      name: "Dirección",
      selector: (row) => row.address,
    },
    {
      name: "Fecha de registro",
      selector: (row) => formatDate(row.registerDate),
    },
    {
      name: "Whatsapp",
      cell: (row) => {
        return (
          <>
            <a
              href={`https://wa.me/506${row.phone}`}
              target="_blank" rel='noreferrer'
              title="Ir a whatsapp"
              type="button"
              className="btn btn-success btn-sm text-white"
            >
               <WhatsAppIcon/>
            </a>
          </>
        );
      },
      ignoreRowClick: true,
      allowOverflow: true,
      button: true,
    },
    {
      name: "Acciones",
      cell: (row) => {
        return (
          <>
          
            <button
              onClick={() => {
                handleDeleteClient(row.clientId, row.name);
              }}
              
              title="Eliminar"
              type="button"
              className="btn btn-danger btn-sm mx-1"
            >
             <i className="ico i__delete"/>
            </button>
            
            <button
              onClick={() => {
                handleEditClient(row.clientId);
              }}
              title="Editar"
              type="button"
              className="btn btn-primary btn-sm"
            >
               <i className="ico i__edit"/>
            </button>
            
          </>
        );
      },
      ignoreRowClick: true,
      allowOverflow: true,
      button: true,
    },
  ];

  const handleAddClient = () => {
    nav("/clients/add");
  };

  const handleEditClient = (id) => {
    nav(`/clients/edit/${id}`);
  };

  const handleDeleteClient = (id, name) => {
    promiseAlert(
      "Atención",
      `¿Seguro deseas eliminar este cliente "${name}?, esta acción es irreversible."`
    ).then((data) => {
      if (data.isConfirmed) {
        handleDelete(id);
      }
    });
  };

  const handleFilterChange = (event) => {
    setSearchValue(event.target.value.toLowerCase());
  }

  function search(){
    if(searchValue !== ''){
      const filtered = data.filter(item =>item.name.toLowerCase().includes(searchValue) );
      setFilteredData(filtered);
    }else{
      setFilteredData(data);
    }
  }

  useEffect(() => {
    if (!data) return;

    setFilteredData(data.filter(item =>item.name.toLowerCase().includes(searchValue) ));
  }, [data])
  
  const toolBarData = {
    handleAdd: handleAddClient,
    handleFilterChange,
    searchValue,
    search,
  };


  return (
    <>
    
    <div>
    <h1>Clientes</h1>
    <TableToolbar {...toolBarData} />

      <DataTable 
      {...TABLE_OPTIONS}
      columns={columns}
      data={filteredData}
      progressPending={isLoading}
      progressComponent={<CustomLoader />}
      />
    </div>
    </>
  );
  }
export default ClientsTable;
 