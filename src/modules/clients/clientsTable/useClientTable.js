const useClientTable = () => {

    function formatDate(date) {
      let dateFormat = new Date(date);
  
      let options = {
        year: "numeric",
        month: "2-digit",
        day: "2-digit",
      };
  
      let formattedDate = dateFormat.toLocaleDateString("es-ES", options);
  
      return formattedDate;
    }
  
    return { formatDate }
  }
  
  export default useClientTable