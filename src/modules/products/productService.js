import useFetch from "../../hooks/useFetch"

import { URL } from "../../utils/constants";

const serviceUrl = `${URL}/product`

const ProductService = () => {
    const { fetchData } = useFetch();

    const getProducts = () => {
        return fetchData({ url: `${serviceUrl}` })
    }

    const getProduct = (id) => {
        return fetchData({ url: `${serviceUrl}/${id}` })
    }

    const addProduct = (data) => {
        const options = { method: 'POST', body: JSON.stringify(data) }
        return fetchData({ url: `${serviceUrl}`, options });
    }

    const updateProduct = (data) => {
        const { id } = data;
        const options = { method: 'PUT', body: JSON.stringify(data) }
        return fetchData({ url: `${serviceUrl}/${id}`, options });
    }

    const deleteProduct = (id) => {
        const options = { method: 'DELETE' }
        return fetchData({ url: `${serviceUrl}/${id}`, options });
    }

    return { getProducts, getProduct, addProduct, updateProduct, deleteProduct };

}

export default ProductService;