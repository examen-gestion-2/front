/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";
import useAlerts from "../../../hooks/useAlerts";

import {
  categoryErrors,
  descriptionErrors,
  discountErrors,
  imageErrors,
  ingredientsErrors,
  nameErrors,
  priceErrors,
  validateCategory,
  validateDescription,
  validateDiscount,
  validateImage,
  validateIngredients,
  validateName,
  validatePrice,
} from "../productsValidator";
import useProductsMutations from "../useProductsMutation";
import { useNavigate } from "react-router";
import useFormHandler from "../../../hooks/useFormHandler";

export const newFormValues = {
  id: 0,  name: "",  image: "",
  description: "",  price: 0,
  category: "",  ingredients: "", discount: 0
};

export const newValidValues = {
  name: true,  image: true,
  description: true,  price: true,
  category: true,  ingredients: true, discount: true,
};

const ingredients_list = ["Pepino", "Kanicama", "Zanaohria", "Pollo", "Atún"];
const categories_list = ["Promoción", "Rollo", "Refresco", "Acompañamiento", "Sopas", "Combos"];

const useProductsModal = (editValues = newFormValues) => {

  const formHandler = useFormHandler(editValues ? {...editValues, ingredients: ""} : newFormValues)
  const { state } = formHandler;

  const name = formHandler.getState('name').state;
  const image = formHandler.getState('image').state;
  const description = formHandler.getState('description').state;
  const price = formHandler.getState('price').state;
  const category = formHandler.getState('category').state;
  const discount = formHandler.getState('discount').state;

  const nav = useNavigate();
  const { toastAlert, promiseAlert } = useAlerts();
  const { handleAdd, handleUpdate } = useProductsMutations();

  const [ingredients, setIngredients] = useState([]);

  const ingredientsBlur = (e) => {
    const input = e.target.value;
    if (input === "") return;

    if (!ingredients_list.includes(input)) {
      toastAlert("Selecciona un ingrediente de la lista");
    } else {
      ingredients.push(input);
    }
    formHandler.updateState("", "ingredients");
  };

  const inputData = {
    name: {
      inputLabel: "Nombre",
      inputType: "Text",
      inputName: "name",
      updateState: formHandler.updateState,
      getState: formHandler.getState,

    },

    price: {
      inputLabel: "Precio",
      inputType: "number",
      inputName: "price",
      updateState: formHandler.updateState,
      getState: formHandler.getState,
    },

    description: {
      inputLabel: "Descripción",
      inputType: "Text",
      inputName: "description",
      updateState: formHandler.updateState,
      getState: formHandler.getState,

    },

    category: {
      inputLabel: "Categoría",
      inputType: "Text",
      inputName: "category",
      updateState: formHandler.updateState,
      getState: formHandler.getState,
      items: categories_list,
    },

    ingredients: {
      inputLabel: "Ingredientes",
      inputType: "Text",
      inputName: "ingredients",
      updateState: formHandler.updateState,
      getState: formHandler.getState,
      items: ingredients_list.filter((i) => !ingredients.includes(i)),
    },

    discount: {
      inputLabel: "Descuento",
      inputType: "Number",
      inputName: "discount",
      updateState: formHandler.updateState,
      getState: formHandler.getState,
    },
  };

  useEffect(() => {
    if (editValues.ingredients.length > 0) {
      setIngredients(editValues.ingredients.split(","));
    }
  }, []);

  useEffect(() => {
    const code = validateName(name)
    const message = nameErrors[code]
    formHandler.updateMessage(message, 'name')
  }, [state.name.state])

  useEffect(() => {
    const code = validateCategory(category)
    const message = categoryErrors[code]
    formHandler.updateMessage(message, 'category')
  }, [state.category.state])

  useEffect(() => {
    const code = validateDescription(description)
    const message = descriptionErrors[code]
    formHandler.updateMessage(message, 'description')
  }, [state.description.state])

  useEffect(() => {
    const code = validatePrice(price)
    const message = priceErrors[code]
    formHandler.updateMessage(message, 'price')
  }, [state.price.state])

  useEffect(() => {
    const code = validateImage(image)
    const message = imageErrors[code]
    formHandler.updateMessage(message, 'image')
  }, [state.image.state])

  useEffect(() => {
    const code = validateIngredients(ingredients.join())
    const message = ingredientsErrors[code]
    formHandler.updateMessage(message, 'ingredients')
  }, [state.ingredients.state])

  useEffect(() => {
    const code = validateDiscount(discount)
    const message = discountErrors[code]
    formHandler.updateMessage(message, 'dicount')
  }, [state.discount.state])

  const handleSubmit = (e) => {
    e.preventDefault();

    if (formHandler.getState("id").state > 0 && JSON.stringify(formHandler.getJson()) === JSON.stringify({...editValues, ingredients: ""})) {
      toastAlert("No se detectaron modificaciones.");
      nav(-1)
      return
    }

    if (
      validateName(name) ||
      validateCategory(category) ||
      validateDescription(description) ||
      validatePrice(price) ||
      validateImage(image) ||
      validateIngredients(ingredients.join()) ||
      validateDiscount(discount)
    ) {
      toastAlert("Datos no válidos.", "error");
      return;
    }

    if (formHandler.getState("id").state > 0) {
      handleUpdate({ ...formHandler.getJson(), ingredients: ingredients.join(), productId: formHandler.getState("id").id });
    } else {
      handleAdd({ ...formHandler.getJson(), ingredients: ingredients.join() })
    }
  };

  const handleCancel = () => {
    if (
      JSON.stringify(formHandler.getJson()) === JSON.stringify({newFormValues}) ||
      JSON.stringify(formHandler.getJson()) === JSON.stringify({...editValues, ingredients: ""})
    ) {
      nav(-1);
      return;
    }

    promiseAlert(
      "Atención",
      `Se perderán los datos ${formHandler.getState("id").id > 0 ? "modificados" : "guardados"
      } ¿Desea continuar?`
    ).then((data) => {
      if (data.isConfirmed) {
        nav(-1);
        return;
      }
    });
  };

  return { inputData, handleSubmit, handleCancel, ingredientsBlur, ingredients, setIngredients, formHandler }
}

export default useProductsModal