import { Fragment, useEffect, useState } from "react";
import useAlerts from "../../../hooks/useAlerts";
import useImageSystem from "../../../hooks/useImageSystem";
import placeholder from "./image_placeholder.jpg";

const ModalIngredientsImage = ({ updateState, getState }) => {
  const { downscaleImage } = useImageSystem();
  const { toastAlert } = useAlerts();
  const [dirty, setDirty] = useState(false);

  useEffect(() => {
    if(getState("image").state !== ""){
      setDirty(true)
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getState("image").state]);

  const handlePicture = async (event) => {
    const file = event.target.files[0];

    if (!file) {
      updateState("", "image");
      return;
    }

    if (!file.type.includes("image")) {
      return toastAlert("No es una imagen válida", "error");
    }

    const resized = await downscaleImage(file);
    updateState(resized, "image");
   
  };

  return (
    <Fragment>
      {getState("image").message && <i className="input-error-image">{dirty && getState("image").message} </i>}

      <label htmlFor="product-image">
        <img
          className="modal__product-image"
          src={getState("image").state ? getState("image").state : placeholder}
          alt="Imagen de producto"
        ></img>
        <p>🖼️ {getState("image").state ? "Modificar" : "Agregar "} </p>
      </label>
      <input
        hidden
        className="input__hidden"
        type="file"
        id="product-image"
        onChange={handlePicture}
      ></input>
     
    </Fragment>
  );
};

export default ModalIngredientsImage;