import React from "react";
import ModalIngredientsRow from "./ModalIngredientsRow";

const ModalIngredientsTable = ({ ingredients, setIngredients }) => {
    
  const handleRemoveIngredient = (e) => {
    const ingredient = e.target.dataset.id;
    ingredients.splice(ingredient, 1);
    setIngredients([...ingredients]);
  };

  return (
    <div className="modal__ingredients-table">
      <table className="table table-bordered table-striped ">
        <tbody className="">
          {ingredients.map((ingredient, index) => 
            <ModalIngredientsRow key={index}  {...{index, ingredient, handleRemoveIngredient}}/>
          )}
        </tbody>
      </table>
    </div>
  );
};

export default ModalIngredientsTable;