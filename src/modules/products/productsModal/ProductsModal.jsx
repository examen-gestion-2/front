import "./productsModal.scss";

import FloatingInputNumeral from "../../../shared/components/FloatingInputNumeral";
import FloatingInput from "../../../shared/components/FloatingInput";
import Datalist from "../../../shared/components/Datalist";
import Textarea from "../../../shared/components/Textarea";

import useProductsModal, { newFormValues } from "./useProductsModal";
import CustomLoader from "../../../shared/components/CustomLoader";
import Modal from "../../../shared/components/Modal";
import Button from "../../../shared/components/Button";
import ModalIngredientsTable from "./ModalIngredientsTable";
import ModalIngredientsImage from "./ModalIngredientsImage";

const ProductsModal = ({ productFetch = null, editValues = newFormValues }) => {
  const {
    inputData,
    handleSubmit,
    handleCancel,
    ingredientsBlur,
    ingredients, 
    setIngredients,
    formHandler
  } = useProductsModal(editValues);

  const buttonsData = {
    cancel: {color:"danger",type:"button", text:"Cancelar", handler:handleCancel},
    submit: {color:"primary",type:"submit", text: formHandler.getState("id").state > 0 ? "Modificar" : "Registrar"}
  }
    
  return (
    <Modal handleCancel={handleCancel} visible={true}>
      {productFetch && !productFetch.data ? (
        <CustomLoader></CustomLoader>
      ) : (
        
        <form onSubmit={handleSubmit}>
          <h4 className="login-form__title mt-3">
            {formHandler.getState("id").state > 0 ? "Modificar" : "Agregar"} producto
          </h4>
            
          <div className="row">
              <div className="col">
                  <FloatingInput {...inputData.name} />
                  <Datalist {...inputData.category}></Datalist>
                  <div>
                 <Textarea {...inputData.description}></Textarea>
                  </div>
              </div>
              <div className="col-sm-8 col-md-4">
                  <FloatingInputNumeral {...inputData.price} />
                  <FloatingInputNumeral {...inputData.discount} min={0} max={100} />
                  <div className="modal__image-container">
                  <ModalIngredientsImage getState={formHandler.getState} updateState={formHandler.updateState}/>
                  </div>
              </div>
              <div className="col">
                  <Datalist
                    {...inputData.ingredients}
                    overridesOnBlur={ingredientsBlur}
                  ></Datalist>
                  <ModalIngredientsTable ingredients={ingredients} setIngredients={setIngredients}/>

                  <div className="modal__action-container">
                    <Button {...buttonsData.cancel}/>
                    <Button {...buttonsData.submit}/>  
                  </div>

              </div>
          </div>

          
        </form>
      
      )}
    </Modal>
  );
};

export default ProductsModal;