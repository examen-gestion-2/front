import React from "react";

const ModalIngredientsRow = ({index, ingredient, handleRemoveIngredient}) => {
  return (
    <tr>
      <td>{ingredient}</td>
      <td data-id={index} onClick={handleRemoveIngredient}>❌</td>
    </tr>
  );
};

export default ModalIngredientsRow;
