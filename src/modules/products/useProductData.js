import { useQuery } from "react-query";
import { MODULE_QUERY_NAMES } from "../../utils/constants";
import ProductService from "./productsService";

// Obtains an unique product to edit.
const useProductData = (id) => {

    const { getProduct } = ProductService();

    const queryOptions = {
        enabled: false,
        cacheTime: 0,
    };

    const { isLoading, isFetching, error, data, refetch } = useQuery(
        [MODULE_QUERY_NAMES.PRODUCTS.ONE, { id }],
        () => { if (id) return getProduct(id) },
        {
            refetchIntervalInBackground: false,
            refetchOnWindowFocus: false,
            ...queryOptions
        }
    );

    return { isLoading, isFetching, error, data, refetch }
}

export default useProductData