import { useEffect, useState } from "react";

import DataTable from "react-data-table-component";
import CustomLoader from "../../../shared/components/CustomLoader";

import useProductsData from "../useProductsData";
import useProductsMutations from "../useProductsMutation";
import useAlerts from "../../../hooks/useAlerts";
import { useNavigate } from "react-router";
import Image from "../../../shared/components/Image";
import Modal from "../../../shared/components/Modal";
import TableToolbar from "../../../shared/components/TableToolbar";
import { TABLE_OPTIONS } from "../../../utils/constants";

const ProductsTable = () => {
  const [visible, setVisible] = useState(false);

  const [selectedImage, setSelectedImage] = useState(-1);

  const { handleDelete } = useProductsMutations();
  const { isLoading, data } = useProductsData();

  const { promiseAlert } = useAlerts();

  const nav = useNavigate();

  const [filteredData, setFilteredData] = useState([]);
  const [searchValue, setSearchValue] = useState("");

  const openImage = (id) => {
    setSelectedImage(id);
    setVisible(true);
  };

  useEffect(() => {
    setFilteredData(data);
  }, [data]);

  const columns = [
    {
      name: "Nombre",
      selector: (row) => row.name,
    },
    {
      name: "Descripcion",
      selector: (row) => row.description,
    },
    {
      name: "Precio",
      cell: (row) => {
        return (
          <>
            {row.discount === 0 ? (
              <>₡ {Number(row.price).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</>
            ) : (
              <>
                <strike> ₡ {Number(row.price).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</strike>
                /₡
                {Number(row.price * (1 - row.discount / 100)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
              </>
            )}
          </>
        );
      },
    },
    {
      name: "Descuento",
      selector: (row) => row.discount + "%",
    },
    {
      name: "Categoria",
      selector: (row) => row.category,
    },
    {
      name: "Ingredientes",
      selector: (row) => row.ingredients,
    },

    {
      name: "Imagen",
      cell: (row) => {
        return (
          <>
            <button
              onClick={() => openImage(row.productId)}
              className="btn btn-warning btn-rounded btn-sm fw-bold"
            >
              {" "}
              <i className="ico i__add" />{" "}
            </button>
          </>
        );
      },
      ignoreRowClick: true,
      allowOverflow: true,
      button: true,
    },
    {
      name: "Acciones",
      cell: (row) => {
        return (
          <>
            <button
              onClick={() => {
                handleDeleteProduct(row.productId, row.name);
              }}
              type="button"
              className="btn btn-danger btn-sm mx-1"
            >
              <i className="ico i__delete" />
            </button>

            <button
              onClick={() => {
                handleEditProduct(row.productId);
              }}
              type="button"
              className="btn btn-primary btn-sm"
            >
              <i className="ico i__edit" />
            </button>
          </>
        );
      },
      ignoreRowClick: true,
      allowOverflow: true,
      button: true,
    },
  ];

  const handleAddProduct = () => {
    nav("/products/add");
  };

  const handleEditProduct = (id) => {
    nav(`/products/edit/${id}`);
  };

  const handleDeleteProduct = (id, name) => {
    promiseAlert(
      "Atención",
      `¿Seguro deseas eliminar el producto "${name}?, esta acción es irreversible."`
    ).then((data) => {
      if (data.isConfirmed) {
        handleDelete(id);
      }
    });
  };

  const handleFilterChange = (event) => {
    //Obtener los cambios del input buscar
    setSearchValue(event.target.value.toLowerCase());
  };

  function search() {
    if (searchValue !== "") {
      const filtered = data.filter((item) =>
        item.name.toLowerCase().includes(searchValue)
      );
      setFilteredData(filtered);
    } else {
      setFilteredData(data);
    }
  }

  const toolBarData = {
    handleAdd: handleAddProduct,
    handleFilterChange,
    searchValue,
    search,
  };

  return (
    <>
      <Modal
        visible={visible}
        handleCancel={() => {
          setVisible(false);
        }}
      >
        <Image id={selectedImage} />
      </Modal>
      <div>
        <h1>Productos</h1>

        <TableToolbar {...toolBarData} />

        <DataTable
          {...TABLE_OPTIONS}
          columns={columns}
          data={filteredData}
          progressPending={isLoading}
          progressComponent={<CustomLoader />}
        />
      </div>
    </>
  );
};
export default ProductsTable;
