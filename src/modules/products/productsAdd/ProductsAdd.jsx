import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router';
import ProductsModal from '../productsModal/ProductsModal'

import { newFormValues } from "../productsModal/useProductsModal";
import useProductData from '../useProductData';

const ProductsAdd = () => {

  const { id } = useParams();
  const [visible, setVisible] = useState(true);
  const [editProduct, setEditProduct] = useState(newFormValues);
  

  const validateProduct = useProductData(id);

  useEffect(() => {
    validateProduct.refetch();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  useEffect(() => {
    if (validateProduct.data && !validateProduct.data.error) {
      const productData = {
        id: validateProduct.data.productId,
        name: validateProduct.data.name,
        image: validateProduct.data.image,
        description: validateProduct.data.description,
        price: validateProduct.data.price,
        category: validateProduct.data.category,
        ingredients: validateProduct.data.ingredients,
        discount: validateProduct.data.discount,
      };

      setEditProduct(productData)
    }
  }, [validateProduct.data]);

  return (
    <>
        <ProductsModal
          productFetch = {id ? validateProduct : null}
          key={new Date().getTime()}
          visible={visible}
          setVisible={setVisible}
          editValues={editProduct}
      ></ProductsModal>
    </>
  )
}

export default ProductsAdd