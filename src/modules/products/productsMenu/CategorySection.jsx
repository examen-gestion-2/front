import React from 'react'
import Image from '../../../shared/components/Image'

const CategorySection = ({title, id, products, openImage}) => {

  return (
    <>
            {products && (<h2 className="category-title" id={id}>{title}</h2>)}
             
             {products && products.map((product, index) => (
                 <div key={index} className="col-13 menu-col" onClick={() => openImage(product.productId)}>

                     <div className="card__container" >
                         <div className="photo">
                             <Image id={product.productId} />
                             <div className="category">{product.category}</div>
                         </div>
                         <div className="content">
                             <p className="txt4">{product.name}</p>
                             <p className="txt3">{product.ingredients}</p>
                             <p className="txt2">{product.description}</p>
                         </div>
                         <div className="footer card__price">
                             <div className="footer__box">
                                 <p className="txt5">₡ { Number(parseInt(product.price * (1 - product.discount / 100))).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</p>
                             </div>
                         </div>
                         <div className="footer card__more">
                             <div className="footer__box">
                                 <p className="txt5">+</p>
                             </div>
                         </div>

                        {product.discount > 0 && (
                        <>
                            <div className="card__old-price">
                                <div className="footer__box">
                                    <p className="txt5 text-decoration-line-through text-danger">
                                    ₡ { Number(product.price).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") }
                                    </p>
                                </div>
                            </div>

                            <div className="footer card__percentage">
                                <div className="footer__box">
                                    <p className="txt5">-{product.discount}%</p>
                                </div>
                            </div>
                        </>
                        )} 
                        
                     </div>
                 </div>
             ))}
    </>
  )
}

export default CategorySection