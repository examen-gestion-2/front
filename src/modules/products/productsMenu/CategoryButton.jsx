import React from 'react'

const CategoryButton = ({link, name}) => {
  return (
    <button className="btn btn-category combo-color-tex col"><a className="category-link" href={link}>{name}</a></button>
  )
}

export default CategoryButton