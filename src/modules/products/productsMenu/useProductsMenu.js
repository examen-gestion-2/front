import { useState } from 'react'
import useProductsData from '../useProductsData';

const useProductsMenu = () => {

    let promoProducts = null;
    let comboProducts = null;
    let rollsProducts = null;
    let acompañamientoProducts = null;
    let sopasProducts = null;
    let refrescoProducts = null;

    const [visible, setVisible] = useState(false);
    const [selectedImage, setSelectedImage] = useState(-1);

    const openImage = (id) => {
        setSelectedImage(id);
        setVisible(true);
    };

    const { data } = useProductsData();

    function lodaData() {
        if (data != null) {
            promoProducts = data.filter(product => product.category === "Promociones" || product.category.toLowerCase() === "promo");
            comboProducts = data.filter(product => product.category === "Combo" || product.category.toLowerCase() === "combo");
            rollsProducts = data.filter(product => product.category === "Rolls" || product.category.toLowerCase() === "rolls");
            acompañamientoProducts = data.filter(product => product.category === "Acompañamiento");
            sopasProducts = data.filter(product => product.category === "Sopas");
            refrescoProducts = data.filter(product => product.category === "Refresco");
        }
    }

    lodaData();

    const categoryLinks = [
        {link: "#promo", name:"Promociones"},
        {link: "#rolls", name:"Rollos Tradicionales"},
        {link: "#acompañamiento", name:"Rollos Especiales"},
        {link: "#combo", name:"Combos"},
        {link: "#sopas", name:"Otros Platillos"},
        {link: "#refresco", name:"Refresco"} 
    ]

    const categoryContent = [
        {products: promoProducts, title:"Promociones", id:"promo" },
        {products: rollsProducts, title:"Rollos Tradicionales" , id:"rolls" },
        {products: acompañamientoProducts, title:"Rollos Especiales" , id:"acompañamiento" },
        {products: comboProducts, title:"Combos", id:"combo" },
        {products: sopasProducts, title:"Otros Platillos" , id:"sopas" },
        {products: refrescoProducts, title:"Refrescos" , id:"refresco" } 
    ]

  return {visible, setVisible, categoryContent, categoryLinks, openImage, selectedImage}
}

export default useProductsMenu