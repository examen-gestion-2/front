

import Image from "../../../shared/components/Image";
import Modal from "../../../shared/components/Modal"

import logo from "./logo.png";

import "./productsMenu.scss";
import CategoryButton from "./CategoryButton";
import CategorySection from "./CategorySection";
import useProductsMenu from "./useProductsMenu";

const ProductsMenu = () => {

    const {visible, setVisible, categoryContent, categoryLinks, openImage, selectedImage} = useProductsMenu();

    return <>
            <Modal className="product-image-modal"
                visible={visible}
                handleCancel={() => {
                    setVisible(false);
                }}
            >
                <Image className="modal-image" id={selectedImage} />
            </Modal>

            <div className="text-center">
                <div className="banner">
                    <img className="menu-logo" src={logo} alt="" />
                </div>

                { categoryContent[0].products !== null && (
                    <div className="container row-category ">
                        {categoryLinks.map((category, i) => <CategoryButton key={i} {...category}/>)}
                    </div>
                )}

                <div className="container">
                    <div className="container row row_menu h-100 d-flex align-items-center justify-content-center ">                    
                        { categoryContent.map((category, i) => <CategorySection key={i} openImage={openImage} {...category} />)}
                    </div>
                </div>
            </div>
        </>
};

export default ProductsMenu;