import { isValidateRequired, isMinLen, isMaxLen, validateArray, isMinNum, isMaxNum } from "../../utils/validations";

const nameValidations = [isValidateRequired, (input) => isMinLen(input, 5), (input) => isMaxLen(input, 25)];
const categoryValidations = [isValidateRequired, (input) => isMinLen(input, 5), (input) => isMaxLen(input, 75)];
const descriptionValidations = [isValidateRequired, (input) => isMinLen(input, 5), (input) => isMaxLen(input, 75)];
const priceValidations = [(input) => isMinNum(input, 0), (input) => isMaxNum(input, 75000)];
const imageValidations = [isValidateRequired];
const ingredientsValidations = [isValidateRequired];

export const nameErrors = {
    REQUIRED: "El nombre es requerido",
    MINLEN: "Se requieren al menos 5 carácteres",
    MAXLEN: "El máximo es de 25 carácteres"
}

export const validateName = (input) => {
    return validateArray(input, nameValidations)
}

export const categoryErrors = {
    REQUIRED: "La categoria es requerida",
    MINLEN: "Se requieren al menos 5 carácteres",
    MAXLEN: "El máximo es de 25 carácteres"
}

export const validateCategory = (input) => {
    return validateArray(input, categoryValidations)
}

export const descriptionErrors = {
    REQUIRED: "La descripcion es requerida",
    MINLEN: "Se requieren al menos 5 carácteres",
    MAXLEN: "El máximo es de 25 carácteres"
}

export const validateDescription = (input) => {
    return validateArray(input, descriptionValidations)
}

export const priceErrors = {
    MINNUM: "El precio no puede ser menor a 0",
    MAXNUM: "El precio no puede ser superior a 75.000"
}

export const validatePrice = (input) => {
    return validateArray(parseInt(input), priceValidations)
}

export const imageErrors = {
    REQUIRED: "La imagen es requerida"
}

export const validateImage = (input) => {
    return validateArray(input, imageValidations)
}

export const ingredientsErrors = {
    REQUIRED: "Debe ingresar al menos un ingrediente"
}

export const validateIngredients = (input) => {
    return validateArray(input, ingredientsValidations)
}

export const discountErrors = {
    MINNUM: "El descuento no puede ser menor a 0",
    MAXNUM: "El descuento no puede ser superior a 100"
}

export const validateDiscount = (input) => {
    return validateArray(input, ingredientsValidations)
}