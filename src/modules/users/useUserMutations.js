import useMutations from "../../hooks/useMutations";
import { MODULE_QUERY_NAMES } from "../../utils/constants";
import UserService from "./userService";

import useAlerts from "../../hooks/useAlerts";

const useUserMutations = () => {
    const {toastAlert} = useAlerts();

    const { addUser, deleteUser, updateUser } = UserService();

    const onSuccessDelete = () => toastAlert("Se eliminó exitosamente", "success");
    const onSuccessAdd = () => toastAlert("Se agregó exitosamente", "success");

    // Base mutations
    const { handleAdd, handleDelete, handleUpdate } = useMutations(
        {
            moduleName:MODULE_QUERY_NAMES.USERS.ALL, 
            addFunction:addUser, 
            deleteFunction:deleteUser, 
            updateFunction:updateUser,
            onSuccessDelete,
            onSuccessAdd
        });

    return { handleAdd, handleDelete, handleUpdate }
}

export default useUserMutations