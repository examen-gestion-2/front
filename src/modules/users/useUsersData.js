import { useQuery } from "react-query";
import { MODULE_QUERY_NAMES } from "../../utils/constants";

import UserService from "./userService";

const useUsersData = () => {

    const { getUsers } = UserService();

    const queryOptions = { cacheTime: 10000, staleTime: 0, refetchInterval: 10000, refetchIntervalInBackground: true };

    const { isLoading, isFetching, error, data } = useQuery(MODULE_QUERY_NAMES.USERS.ALL, getUsers, queryOptions);

    return { isLoading, isFetching, error, data }
}

export default useUsersData