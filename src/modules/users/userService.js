import useFetch from "../../hooks/useFetch"

import { URL } from "../../utils/constants";

const serviceUrl = `${URL}/user`

const UserService = () => {
    const { fetchData } = useFetch();

    const getUsers = () => {
        return fetchData({ url: `${serviceUrl}` })
    }

    const getUser = (id) => {
        return fetchData({ url: `${serviceUrl}/${id}` })
    }

    const addUser = (data) => {
        const options = { method: 'POST', body: JSON.stringify(data) }
        return fetchData({ url: `${serviceUrl}`, options });
    }

    const updateUser = (data) => {
        const { id } = data;
        const options = { method: 'PUT', body: JSON.stringify(data) }
        return fetchData({ url: `${serviceUrl}/${id}`, options });
    }

    const deleteUser = (id) => {
        const options = { method: 'DELETE' }
        return fetchData({ url: `${serviceUrl}/${id}`, options });
    }

    return { getUsers, getUser, addUser, updateUser, deleteUser };

}

export default UserService;