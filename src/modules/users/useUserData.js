import { useQuery } from "react-query";
import { MODULE_QUERY_NAMES } from "../../utils/constants";

import UserService from "./userService";

const useUsersData = (userId) => {

    const { getUser } = UserService();

    const queryOptions = { cacheTime: 10000, staleTime: 0, refetchInterval: 10000, refetchIntervalInBackground: true };

    const { isLoading, isFetching, error, data } =  useQuery([MODULE_QUERY_NAMES.USERS.ONE,userId], () => getUser(userId), queryOptions); 

    return { isLoading, isFetching, error, data }
}

export default useUsersData