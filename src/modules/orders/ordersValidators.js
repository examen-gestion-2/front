import { isValidateRequired, isMinLen, isMaxLen, validateArray, isMinNum, isMaxNum } from "../../utils/validations";

const registerDateValidations = [isValidateRequired, (input) => isMinNum(input, 0), (input) => isMaxNum(input, 75000)];
const paymentMethodValidations = [isValidateRequired, (input) => isMinLen(input, 0), (input) => isMaxLen(input, 8)];
const emballageCostValidations = [isValidateRequired, (input) => isMinLen(input, 0), (input) => isMaxLen(input, 75)];
const shippingCostValidations = [isValidateRequired, (input) => isMinLen(input, 0), (input) => isMaxLen(input, 25)];
const totalValidations = [isValidateRequired, (input) => isMinLen(input, 0), (input) => isMaxLen(input, 100000)];
const clientIdValidations = [isValidateRequired];;

export const registerDateErrors = {
    MINNUM: "La fecha de registro es necesaria",
    MAXNUM: "La fecha de registro es necesaria"
}

export const validateRegisterDate = (input) => {
    return validateArray(input, registerDateValidations)
}

export const paymentMethodErrors = {
    MINNUM: "Metodo de pago es necesario",
    MAXNUM: "Metodo de pago es necesario"
}

export const validatePaymentMethod = (input) => {
    return validateArray(input, paymentMethodValidations)
}

export const emballageCostErrors = {
    MINNUM: "El costo de empaque es necesario",
    MAXNUM: "El costo de empaque es necesario"
}

export const validateEmballageCost = (input) => {
    return validateArray(input, emballageCostValidations)
}

export const shippingCostErrors = {
    MINNUM: "El costo de envió es necesario",
    MAXNUM: "El costo de envió es necesario"
}

export const validateShippingCost = (input) => {
    return validateArray(input, shippingCostValidations)
}

export const totalErrors = {
    MINNUM: "El total del pedido es necesario",
    MAXNUM: "El total del pedido es necesario"
}

export const validateTotal = (input) => {
    return validateArray(input, totalValidations)
}

export const clientIdErrors = {
    REQUIRED: "Solo se permite ingresar 1 cliente"
}

export const validateClientId = (input) => {
    return validateArray(input, clientIdValidations)
}