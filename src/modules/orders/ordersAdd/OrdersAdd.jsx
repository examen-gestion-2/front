import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router';
import OrdersModal from '../ordersModal/OrdersModal'

import { newFormValues } from "../ordersModal/useOrdersModal"; 
import useOrderData from '../useOrderData'; //acá

const OrdersAdd = () => {

  const { id } = useParams(); 
  const [visible, setVisible] = useState(true);
  const [editOrder, setEditOrder] = useState(newFormValues);

  const validateOrder = useOrderData(id);

  useEffect(() => {
    validateOrder.refetch();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  useEffect(() => {
    if (validateOrder.data && !validateOrder.data.error) {
      const orderData = {
        id: validateOrder.data.orderId,
        registerDate: validateOrder.data.registerDate, 
        paymentMethod: validateOrder.data.paymentMethod,
        emballageCost: validateOrder.data.emballageCost,
        shippingCost: validateOrder.data.shippingCost,
        total: validateOrder.data.total,
        clientId: validateOrder.data.clientId
      };

      setEditOrder(orderData)
    }
  }, [validateOrder.data]);

  return (
    <>
        <OrdersModal
          productFetch = {id ? validateOrder : null}
          key={new Date().getTime()}
          visible={visible}
          setVisible={setVisible}
          editValues={editOrder}
      ></OrdersModal>
    </>
  )
}

export default OrdersAdd