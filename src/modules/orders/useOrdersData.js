import { useQuery } from "react-query";
import { MODULE_QUERY_NAMES } from "../../utils/constants";
import OrderService from "./ordersService";

const useOrdersData = () => {

    const { getOrders } = OrderService();

    const queryOptions = { retry: 1, cacheTime: 10000, staleTime: 0, refetchInterval: 10000, refetchIntervalInBackground: true, enabled: true };

    const { isLoading, isFetching, error, data } = useQuery(MODULE_QUERY_NAMES.ORDERS.ALL, getOrders, queryOptions);

    return { isLoading, isFetching, error, data }
}

export default useOrdersData