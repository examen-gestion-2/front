import useFetch from "../../hooks/useFetch"

import { URL } from "../../utils/constants";

const serviceUrl = `${URL}/order`

const OrderService = () => {
    const { fetchData } = useFetch();

    const getOrders = () => {
        return fetchData({ url: `${serviceUrl}` })
    }

    const getOrder = (id) => {
        console.log(id,"GA")
        return fetchData({ url: `${serviceUrl}/${id}` })
    }

    const addOrder = (data) => {
        const options = { method: 'POST', body: JSON.stringify(data) }
        return fetchData({ url: `${serviceUrl}`, options });
    }

    const updateOrder = (data) => {
        const { id } = data;
        const options = { method: 'PUT', body: JSON.stringify(data) }
        return fetchData({ url: `${serviceUrl}/${id}`, options });
    }

    const deleteOrder = (id) => {
        const options = { method: 'DELETE' }
        return fetchData({ url: `${serviceUrl}/${id}`, options });
    }

    return { getOrders, getOrder, addOrder, updateOrder, deleteOrder };

}

export default OrderService;