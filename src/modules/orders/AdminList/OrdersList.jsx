import React from "react";

import orderStatus from "../order_status.json";

const OrdersList = ({ title, orders, openOrder, emptyText }) => {
  return (
    <div>
      <div className="row_order">
        <h2 className="" id={title}>{title}</h2>
        <hr />
      </div>

      <div className="row row_order text-center">
        {orders &&
          orders.map((order, index) => (
            <div key={index} className="col-sm-3 order-col order-col-detail">
              <div className="card__container-order">
                <div className="row container-fluid">
                  <h2 className="orders-title">{order.orderId}</h2>
                  <h4 className="txt3-order date-order">
                    {order.registerDate}
                  </h4>
                </div>
                <div className="content-order">
                  <p className="txt4-order">{order.name}</p>
                  <p className="txt3-order">{order.address}</p>
                </div>
                <button
                  className={
                    "footer card__footer-orders " +
                    orderStatus[order.state].class
                  }
                  onClick={() => {
                    openOrder(order.orderId);
                  }}
                >
                  <p className="txt5-order ">
                    {orderStatus[order.state].title}
                  </p>
                </button>
              </div>
            </div>
          ))}

        {
          orders.length === 0 && 
            <h2> No hay ordenes {emptyText}</h2>
        }
          
      </div>
    </div>
  );
};

export default OrdersList;