import { Fragment, useState, useEffect } from "react";

import useOrdersData from "../useOrdersData";
import useOrderData from "../useOrderData";
import Modal from "../../../shared/components/Modal";

import StateButton from "./StateButton";
import useAdminListsMenu from "./useAdminList";

import "./OrdersList";
import AdminModal from "./AdminModal";

import OrdersList from "./OrdersList";

import { useNavigate } from "react-router";

const AdminList = () => {
  const [visible, setVisible] = useState(false);
  const [selectedOrder, setSelectedOrder] = useState(0);

  const nav = useNavigate();

  const { stateLinks } = useAdminListsMenu();

  const openOrder = (id) => {
    setSelectedOrder(id);
    setVisible(true);
  };

  const orders = useOrdersData().data;

  let { data, refetch } = useOrderData(selectedOrder);

  useEffect(() => {
    refetch();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedOrder]);

  let sections;
  if (orders) {
    sections = [
      { orders: orders.filter((order) => order.state === 1), title: "Pedidos en proceso", emptyText: "en proceso." },
      { orders: orders.filter((order) => order.state === 0), title: "Pedidos nuevos", emptyText: "nuevas." },
      { orders: orders.filter((order) => order.state === 2), title: "Pedidos entregados", emptyText: "entregadas." },
      { orders: orders.filter((order) => order.state === 1), title: "Pedidos cancelados", emptyText: "canceladas." }
    ]
  }

  const handleAddOrder = () => {
    nav("/orders/add");
  };

  return (
    <Fragment>
      <Modal
        className="order__modal"
        visible={visible}
        handleCancel={() => {
          setVisible(false);
        }}
      >
        <AdminModal data={data} setVisible={setVisible} />
      </Modal>

      {sections != null && (
        <div className="row_order">
          <h1 className="">Pedidos</h1>
          <button
            onClick={handleAddOrder}
            className="btn btn-category combo-color-tex bg-primary text-white col-lg-1"
          >
            <i className="ico i__add" /> Agregar
          </button>

          {stateLinks.map((category, i) => <StateButton key={i} {...category} />)}

        </div>
      )}

      {orders && (
        <>
          {sections.map(section => <OrdersList openOrder={openOrder} {...section} />)}
        </>
      )}
    </Fragment>
  );
};

export default AdminList;
