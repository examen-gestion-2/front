
const useAdminList = () => {

    const stateLinks = [
        {link: "#Pedidos en proceso", name:"Pedidos en proceso"},
        {link: "#Pedidos nuevos", name:"Pedidos nuevos"},
        {link: "#Pedidos entregados", name:"Pedidos entregados"},
        {link: "#Pedidos cancelados", name:"Pedidos cancelados"},
    ]


  return {  stateLinks }
}

export default useAdminList