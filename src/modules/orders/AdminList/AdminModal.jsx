import React from 'react'

import WhatsAppIcon from '@mui/icons-material/WhatsApp';
import orderStatus from "../order_status.json"
import useOrdersMutation from '../useOrdersMutations';
import { PAY_METHODS } from '../../../utils/constants';

const AdminModal = ({data, setVisible}) => {
  
  const { handleUpdate } = useOrdersMutation();

  const onChangeState = (e) => {
    handleUpdate({ id: data.orderId, state: data.state+1 });
    setVisible(false);
  };

  return ( 
    <>
     {
         data &&
        <div className="order__container-detail">
        <h2>#{ data.orderId}</h2>

        <h4 className="txt3-order date-order">{ data.registerDate}</h4>
        <div className="content-order">
          <p className="txt4-order">{ data.name}</p>
          <p className="txt6-order">{ data.address}</p>
        </div>

        <a  href={`https://wa.me/506${data.phone}`} 
        title={`Contactar a ${data.name}`}
        className="order_whatsapp" 
        target="_blank" 
        rel='noreferrer'>{ <WhatsAppIcon/>}</a>

        <div className="row row-detail">
          {
            data.products.map((product, index) => (
              <div key={index} className="col content-order-detail">
                <p className="txt4-order">Productos</p>
                <p className="txt3-order">
                  {product.quantity}{" "}
                  <span className="text-muted">{product.name}</span> ₡{" "}
                  {product.price}
                </p>
              </div>
            ))}
          <div className="col content-order-detail">
            <p className="txt4-order">Detalles</p>

            <p className="txt3-order">
              <span className="text-muted">Costo del envio:</span>{" "}
              { data.shippingCost}
            </p>
            <p className="txt3-order">
              <span className="text-muted">Costo del empaque:</span>{" "}
              { data.emballageCost}
            </p>
            <p className="txt3-order">
            <span className="text-muted">Paga con:</span>{" "}
              { PAY_METHODS[data.paymentMethod]}
            </p>
            <p className="txt3-order">
              <span className="text-muted">Total:</span>{" "}
              <span className="text-danger"><>₡ {Number(data.total).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</></span>
            </p>
          </div>
        </div>
        <div className={"p-1 " + orderStatus[data.state].class} >
          <div className="">
            <p className="txt5-order">{orderStatus[data.state].title}</p>
          </div>
        </div>
        {
          orderStatus[data.state].nextStep && 
          <button className={"footer card__footer-order " + orderStatus[data.state+1].class} onClick={onChangeState}>
            <div className="">
              <p className="txt5-order"> {orderStatus[1].nextStep}</p>
            </div>
          </button>
        }
        

      </div>
     }
        </>
  )
}

export default AdminModal