import { useQuery } from "react-query";
import { MODULE_QUERY_NAMES } from "../../../utils/constants";
import ClientService from "../../clients/clientsService";

const useClientsData = () => {

    const { getClients } = ClientService();

    const queryOptions = { retry: 1, cacheTime: 10000, staleTime: 0, refetchInterval: 10000, refetchIntervalInBackground: true, enabled: true };

    const { data } = useQuery(MODULE_QUERY_NAMES.CLIENTS.ALL, getClients, queryOptions);

    return { data }
}

export default useClientsData