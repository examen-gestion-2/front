import { useEffect, useState } from "react";
import Select from 'react-select'
import makeAnimated from 'react-select/animated';
import "./ordersModal.scss";
import Modal from "../../../shared/components/Modal";
import FloatingInput from "../../../shared/components/FloatingInput";
import useOrdersModal, { newFormValues } from "./useOrdersModal";
import CustomLoader from "../../../shared/components/CustomLoader";
import Button from "../../../shared/components/Button";
import useClientsData from "../ordersModal/clientsData";
import useProductsData from "./productsData";

const OrdersModal = ({ orderFetch = null, editValues = newFormValues }) => { 

  const {data} = useClientsData();
  const [isLoading, setIsLoading] = useState(true);
  const [dataClient, setDataClient] = useState([]);


  const {dataProduct} = useProductsData();
  const [isLoadingProducts, setIsLoadingProducts] = useState(true);
  const [dataProducts, setDataProduct] = useState([]);

  const animatedComponents = makeAnimated();
 
  useEffect(() => {
    if(data !== undefined){
        setDataClient(data); 
        setIsLoading(false); 
      }
  }, [data]);

  useEffect(() => {
    if(dataProduct !== undefined){
        setDataProduct(dataProduct); 
        setIsLoadingProducts(false); 
      }
  }, [dataProduct]);

  const { 
    inputData,
    handleSubmit,
    handleCancel,
    formHandler
  } = useOrdersModal(editValues); 

  const buttonsData = {
    cancel: {color:"danger",type:"button", text:"Cancelar", handler:handleCancel},
    submit: {color:"primary",type:"submit", text: formHandler.getState("id").state > 0 ? "Modificar" : "Registrar"}
  }

  const opcions_clients = [];
  if(dataClient !== undefined){
    dataClient.forEach(element => {
    opcions_clients.push({value:element.clientId, label:element.name});
  });
 }
 const methodPayment = [
      { value: 1, label: 'Efectivo' },
      { value: 2, label: 'Sinpe Móvil'  }];

 const opcions_products = [];
  if(dataProducts !== undefined){
    dataProducts.forEach(element => { 
      opcions_products.push({value:element.productId, label:element.name});
});
}

  if (isLoading===true && isLoadingProducts===true) { 
    return (
      <div className="App">
        <h1>Cargando...</h1>
      </div>
    );
  }
  return (
    <Modal handleCancel={handleCancel} visible={true}>
      {orderFetch && !orderFetch.data ? (
        <CustomLoader></CustomLoader>
      ) : (
        
        <form onSubmit={handleSubmit}>
          <h4 className="login-form__title mt-3">
            {formHandler.getState("id").state > 0 ? "Modificar" : "Agregar"} orden
          </h4>
          <div className="row">
              <div className='col'>
                    <div>Elegir un cliente</div>
                    <Select options={opcions_clients} {...inputData.clientId}/>
                    <div></div>
                    <div>Elegir productos</div>
                    <Select 
                    closeMenuOnSelect={false}
                    components={animatedComponents}
                    defaultValue={[opcions_products[4], opcions_products[5]]}
                    isMulti
                    options={opcions_products} />
              </div>
              <div className="col">
                  <FloatingInput {...inputData.registerDate} />
                  <FloatingInput {...inputData.emballageCost} />
                  <div>Metodo de pago</div>
                  <Select options={methodPayment} />
                  <div></div>
             </div>
             <div className="col">
                  <FloatingInput {...inputData.shippingCost}/>
                  <FloatingInput {...inputData.total}/>
                  <div className="modal__action-container">
                      <Button {...buttonsData.cancel}/>
                      <Button {...buttonsData.submit}/>  
                </div>
            </div>
              </div>
          
        </form>
      
      )}
    </Modal>
  );
};

export default OrdersModal;