/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect} from "react";
import useAlerts from "../../../hooks/useAlerts";

import {
  registerDateErrors,
  paymentMethodErrors,
  emballageCostErrors,
  shippingCostErrors,
  totalErrors,
  clientIdErrors,
  validateRegisterDate,
  validatePaymentMethod,
  validateEmballageCost,
  validateShippingCost,
  validateTotal,
  validateClientId,
} from "../ordersValidators";
import useOrdersMutations from "../useOrdersMutations";
import { useNavigate } from "react-router";
import useFormHandler from "../../../hooks/useFormHandler";

export const newFormValues = {
  id: 0,  registerDate: "", 
  paymentMethod: 0, emballageCost: 0,
  shippingCost: 0, total: 0,
  stateOrder: 1, isActive:1, clientId: 0,
};

export const newValidValues = {
  id: true, registerDate: true, 
  paymentMethod: true, emballageCost: true,
  shippingCost: true, total: true,
  stateOrder: true, isActive: true, clientId: true,
};


const useOrdersModal = (editValues) => {

  const formHandler = useFormHandler(editValues ? {...editValues} : newFormValues) ///Acá
  const { state } = formHandler;
 
  const registerDate = formHandler.getState('registerDate').state;
  const paymentMethod = formHandler.getState('paymentMethod').state;
  const emballageCost = formHandler.getState('emballageCost').state;
  const shippingCost = formHandler.getState('shippingCost').state;
  const total = formHandler.getState('total').state;
  const clientId = formHandler.getState('clientId').state;
  
  const nav = useNavigate();
  const { toastAlert, promiseAlert } = useAlerts();
  const { handleAdd, handleUpdate } = useOrdersMutations();

  const inputData = {
    registerDate: {
      inputLabel: "Fecha de registro",
      inputType: "Date",
      inputName: "registerDate",
      updateState: formHandler.updateState,
      getState: formHandler.getState,
    },
    paymentMethod: {
      inputLabel: "Metódo de pago",
      inputType: "text",
      inputName: "paymentMethod",
      updateState: formHandler.updateState,
      getState: formHandler.getState,
    },

    emballageCost: {
      inputLabel: "Costo de empaque",
      inputType: "Text",
      inputName: "emballageCost",
      updateState: formHandler.updateState,
      getState: formHandler.getState,

    },
    shippingCost: {
      inputLabel: "Costo de envio",
      inputType: "Text",
      inputName: "shippingCost",
      updateState: formHandler.updateState,
      getState: formHandler.getState,

    },
    total: {
      inputLabel: "Total",
      inputType: "Text",
      inputName: "total",
      updateState: formHandler.updateState,
      getState: formHandler.getState,

    },
    stateOrder: {
      inputLabel: "Estado",
      inputType: "Text",
      inputName: "stateOrder",
      updateState: formHandler.updateState,
      getState: formHandler.getState,

    },
    clientId: {
      inputLabel: "Cliente",
      inputType: "Text",
      inputName: "clientID",
      updateState: formHandler.updateState,
      getState: formHandler.getState,
    },
   
  };

  useEffect(() => {
    const code = validateRegisterDate(registerDate)
    const message = registerDateErrors[code]
    formHandler.updateMessage(message, 'registerDate')
  }, [state.registerDate.state])

  useEffect(() => {
    const code = validatePaymentMethod(paymentMethod)
    const message = paymentMethodErrors[code]
    formHandler.updateMessage(message, 'paymentMethod')
  }, [state.paymentMethod.state])

  useEffect(() => {
    const code = validateEmballageCost(emballageCost)
    const message = emballageCostErrors[code]
    formHandler.updateMessage(message, 'emballageCost')
  }, [state.emballageCost.state])

  useEffect(() => {
    const code = validateShippingCost(shippingCost)
    const message = shippingCostErrors[code]
    formHandler.updateMessage(message, 'shippingCost')
  }, [state.shippingCost.state])

  useEffect(() => {
    const code = validateTotal(total)
    const message = totalErrors[code]
    formHandler.updateMessage(message, 'total')
  }, [state.total.state])

  useEffect(() => {
    const code = validateClientId(clientId)
    const message = clientIdErrors[code]
    formHandler.updateMessage(message, 'clientId')
  }, [state.clientId.state])

  const handleSubmit = (e) => {
    e.preventDefault();

    if (
      validateRegisterDate(registerDate) ||
      validatePaymentMethod(paymentMethod) ||
      validateEmballageCost(emballageCost)||
      validateShippingCost(shippingCost) ||
      validateTotal(total) ||
      validateClientId(clientId)
    ) {
      toastAlert("Datos no válidos.", "error");
      return;
    }

    if (formHandler.getState("id").state > 0) {
      handleUpdate({ ...formHandler.getJson(), registerDate: formHandler.getState('registerDate').state, orderId: formHandler.getState("id").id });
    } else {
      handleAdd({ ...formHandler.getJson(), registerDate: formHandler.getState('registerDate').state }) //Acá tengo que mandar 
    }
  };

  const handleCancel = () => {
    if (
      JSON.stringify(formHandler.getJson()) === JSON.stringify({newFormValues}) ||
      JSON.stringify(formHandler.getJson()) === JSON.stringify({...editValues})
    ) {
      nav(-1);
      return;
    }

    promiseAlert(
      "Atención",
      `Se perderán los datos ${formHandler.getState("id").id > 0 ? "modificados" : "guardados"
      } ¿Desea continuar?`
    ).then((data) => { 
      if (data.isConfirmed) {
        nav(-1);
        return;
      }
    });
  };

  return { inputData, handleSubmit, handleCancel,formHandler}
}

export default useOrdersModal