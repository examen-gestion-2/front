import { useQuery } from "react-query";
import { MODULE_QUERY_NAMES } from "../../../utils/constants";
import ProductService from "../../products/productsService";

const useProductsData = () => {

    const { getProducts } = ProductService();

    const queryOptions = { retry: 1, cacheTime: 10000, staleTime: 0, refetchInterval: 10000, refetchIntervalInBackground: true, enabled: true };

    const {data} = useQuery(MODULE_QUERY_NAMES.PRODUCTS.ALL, getProducts, queryOptions);

    const dataProduct = data;
    return { dataProduct }
}

export default useProductsData