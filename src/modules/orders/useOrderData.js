import { useQuery } from "react-query";
import { MODULE_QUERY_NAMES } from "../../utils/constants";
import OrderService from "./ordersService";

// Obtains an unique order to edit.
const useOrderData = (id) => {
    
    const { getOrder } = OrderService();

    const queryOptions = {
        enabled: false,
        cacheTime: 0,
    };

    const { isLoading, isFetching, error, data, refetch } = useQuery(
        [MODULE_QUERY_NAMES.ORDERS.ONE, { id }],
        () => { if (id) return getOrder(id) },
        {
            refetchIntervalInBackground: false,
            refetchOnWindowFocus: false,
            ...queryOptions
        }
    );

    return { isLoading, isFetching, error, data, refetch }
}

export default useOrderData