import { useNavigate } from "react-router-dom";

import useMutations from "../../hooks/useMutations";
import useAlerts from "../../hooks/useAlerts";
import StockService from "./stocksService";
import { MODULE_QUERY_NAMES } from "../../utils/constants";

const useStockMutations = () => {
    const { addStock, deleteStock, updateStock } = StockService();
    const {toastAlert} = useAlerts();
    const navigate = useNavigate();

    const onSuccessDelete = () => toastAlert("Se eliminó exitosamente", "success");
    const onSuccessUpdate = () => {
        toastAlert("Se actualizó exitosamente", "success");
        navigate(-1);
    }
    const onSuccessAdd = () => {
        toastAlert("Se agregó exitosamente", "success");
        navigate(-1);
    };
    const onErrorDelete = () => toastAlert("No se pudo eliminar", "error");
    const onErrorUpdate = () => toastAlert("No se pudo actualizar", "error");
    const onErrorAdd = () => toastAlert("No se pudo agregar", "error");

    // Base mutations
    const { handleAdd, handleDelete, handleUpdate } = useMutations(
        {
            moduleName:MODULE_QUERY_NAMES.STOCKS.ALL, 
            addFunction:addStock, 
            deleteFunction:deleteStock, 
            updateFunction:updateStock,
            onSuccessUpdate,
            onErrorUpdate,
            onSuccessDelete,
            onSuccessAdd,
            onErrorDelete,
            onErrorAdd
        });

    return { handleAdd, handleDelete, handleUpdate }
}

export default useStockMutations