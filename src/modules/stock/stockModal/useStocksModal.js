/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect} from "react";
import useAlerts from "../../../hooks/useAlerts";

import {
  nameErrors,
  quantityErrors,
  priceErrors,
  registerDateErrors,
  validateName,
  validateQuantity,
  validatePrice,
  validateRegisterDate,
} from "../stockValidator";
import useStocksMutations from "../useStockMutation";
import { useNavigate } from "react-router";
import useFormHandler from "../../../hooks/useFormHandler";

export const newFormValues = {
  id: 0,  name: "", quantity: 0, 
   price: 0, registerDate: "",
};

export const newValidValues = {
  name: true, quantity: true, 
  price: true, registerDate: true, 
};

const useStocksModal = (editValues = newFormValues) => {

  const formHandler = useFormHandler(editValues ? {...editValues} : newFormValues) ///Acá
  const { state } = formHandler;

  const name = formHandler.getState('name').state;
  const quantity = formHandler.getState('quantity').state;
  const price = formHandler.getState('price').state;
  const registerDate = formHandler.getState('registerDate').state;
  
  const nav = useNavigate();
  const { toastAlert, promiseAlert } = useAlerts();
  const { handleAdd, handleUpdate } = useStocksMutations();

  const inputData = {
    name: {
      inputLabel: "Nombre",
      inputType: "Text",
      inputName: "name",
      updateState: formHandler.updateState,
      getState: formHandler.getState,

    },

    quantity: {
      inputLabel: "Cantidad",
      inputType: "text",
      inputName: "quantity",
      updateState: formHandler.updateState,
      getState: formHandler.getState,
    },

    price: {
      inputLabel: "Precio",
      inputType: "Text",
      inputName: "price",
      updateState: formHandler.updateState,
      getState: formHandler.getState,

    },

    registerDate: {
      inputLabel: "Fecha de registro",
      inputType: "Date",
      inputName: "registerDate",
      updateState: formHandler.updateState,
      getState: formHandler.getState,
    },
   
  };

  useEffect(() => {
    const code = validateName(name)
    const message = nameErrors[code]
    formHandler.updateMessage(message, 'name')
  }, [state.name.state])

  useEffect(() => {
    const code = validateQuantity(quantity)
    const message = quantityErrors[code]
    formHandler.updateMessage(message, 'quantity')
  }, [state.quantity.state])

  useEffect(() => {
    const code = validatePrice(price)
    const message = priceErrors[code]
    formHandler.updateMessage(message, 'price')
  }, [state.price.state])

  useEffect(() => {
    const code = validateRegisterDate(registerDateErrors)
    const message = registerDateErrors[code]
    formHandler.updateMessage(message, 'registerDate')
  }, [state.registerDate.state])

  const handleSubmit = (e) => {
    e.preventDefault();

    if (
      validateName(name) ||
      validateQuantity(quantity) ||
      validatePrice(price) ||
      validateRegisterDate(registerDate) 
    ) {
      toastAlert("Datos no válidos.", "error");
      return;
    }

    if (formHandler.getState("id").state > 0) {
      handleUpdate({ ...formHandler.getJson(), registerDate: formHandler.getState('registerDate').state, productId: formHandler.getState("id").id });
    } else {
      handleAdd({ ...formHandler.getJson(), registerDate: formHandler.getState('registerDate').state})
    }
  };

  const handleCancel = () => {
    if (
      JSON.stringify(formHandler.getJson()) === JSON.stringify({newFormValues}) ||
      JSON.stringify(formHandler.getJson()) === JSON.stringify({...editValues})
    ) {
      nav(-1);
      return;
    }

    promiseAlert(
      "Atención",
      `Se perderán los datos ${formHandler.getState("id").id > 0 ? "modificados" : "guardados"
      } ¿Desea continuar?`
    ).then((data) => { 
      if (data.isConfirmed) {
        nav(-1);
        return;
      }
    });
  };

  return { inputData, handleSubmit, handleCancel, formHandler}
}

export default useStocksModal