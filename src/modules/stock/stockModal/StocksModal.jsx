import "./StocksModal.scss";

import FloatingInput from "../../../shared/components/FloatingInput";

import useStocksModal, { newFormValues } from "./useStocksModal";
import CustomLoader from "../../../shared/components/CustomLoader";
import Modal from "../../../shared/components/Modal";
import Button from "../../../shared/components/Button";

const StocksModal = ({ stockFetch = null, editValues = newFormValues }) => {
  const { 
    inputData,
    handleSubmit,
    handleCancel,
    formHandler
  } = useStocksModal(editValues);

  const buttonsData = {
    cancel: {color:"danger",name:"btnCancel",type:"button", text:"Cancelar", handler:handleCancel},
    submit: {color:"primary",name:"btnRM",type:"submit", text: formHandler.getState("id").state > 0 ? "Modificar" : "Registrar"}
  }
    
  return (
    <Modal handleCancel={handleCancel} visible={true}>
      {stockFetch && !stockFetch.data ? (
        <CustomLoader></CustomLoader>
      ) : (
        
        <form onSubmit={handleSubmit}>
          <h4 className="login-form__title mt-3">
            {formHandler.getState("id").state > 0 ? "Modificar" : "Agregar"} inventario
          </h4>
          <div className="row">
              <div className="col">
                  <FloatingInput {...inputData.name} />
                  <FloatingInput {...inputData.quantity}/>
                  <FloatingInput {...inputData.price}/>
             </div>
             <div className="col">
                  <div>
                  <FloatingInput {...inputData.registerDate}/>
                  </div>
                  <div className="modal__action-container">
                    <Button {...buttonsData.cancel}/>
                    <Button {...buttonsData.submit}/>  
                  </div>

              </div>
             
          </div>

          
        </form>
      
      )}
    </Modal>
  );
};

export default StocksModal;