import { useQuery } from "react-query";
import { MODULE_QUERY_NAMES } from "../../utils/constants";
import StockService from "./stocksService";

// Obtains an unique client to edit.
const useStockData = (id) => {

    const { getStock } = StockService();

    const queryOptions = {
        enabled: false,
        cacheTime: 0,
    };
    console.log(getStock(id))
    const { isLoading, isFetching, error, data, refetch } = useQuery(
        [MODULE_QUERY_NAMES.STOCKS.ONE, { id }],

        () => { if (id) return getStock(id) },
        {
            refetchIntervalInBackground: false,
            refetchOnWindowFocus: false,
            ...queryOptions
            
        }
        
    );

    return { isLoading, isFetching, error, data, refetch }
}

export default useStockData