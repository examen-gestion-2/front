import { isValidateRequired, isMinLen, isMaxLen, validateArray, isMinNum, isMaxNum } from "../../utils/validations";

const nameValidations = [isValidateRequired, (input) => isMinLen(input, 3), (input) => isMaxLen(input, 25)];
const quantityValidations = [(input) => isMinNum(input, 0), (input) => isMaxNum(input, 75000)];
const priceValidations = [(input) => isMinNum(input, 0), (input) => isMaxNum(input, 200000)];
const registerDateValidations = [isValidateRequired, (input) => isMinNum(input, 0), (input) => isMaxNum(input, 75000)];

export const nameErrors = {
    REQUIRED: "El nombre es requerido",
    MINLEN: "Se requieren al menos 3 carácteres",
    MAXLEN: "El máximo es de 25 carácteres"
}

export const validateName = (input) => {
    return validateArray(input, nameValidations)
}

export const quantityErrors = {
    MINNUM: "La cantidad no puede ser menor a 0",
    MAXNUM: "La cantidad no puede ser superior a 75.000"
}

export const validateQuantity = (input) => {
    return validateArray(parseInt(input), quantityValidations)
}

export const priceErrors = {
    MINNUM: "El precio no puede ser menor a 0",
    MAXNUM: "El precio no puede ser superior a 200.000"
}

export const validatePrice = (input) => {
    return validateArray(parseInt(input), priceValidations)
}

export const registerDateErrors = {
    MINNUM: "Se necesita una fecha de registro",
    MAXNUM: "La fecha de registro no puede ser superior a 75.000"
}

export const validateRegisterDate = (input) => {
    return validateArray(input, registerDateValidations)
}
