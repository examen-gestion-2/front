import useFetch from "../../hooks/useFetch"

import { URL } from "../../utils/constants";

const serviceUrl = `${URL}/stock` ////Acá

const StockService = () => {
    const { fetchData } = useFetch();

    const getStocks = () => {
        return fetchData({ url: `${serviceUrl}` })
    }

    const getStock = (id) => {
        return fetchData({ url: `${serviceUrl}/${id}` })
    }

    const addStock = (data) => {
        const options = { method: 'POST', body: JSON.stringify(data) }
        return fetchData({ url: `${serviceUrl}`, options });
    }

    const depleteStock = (id) => {
        const options = { method: 'POST' }
        return fetchData({ url: `${serviceUrl}/deplete/${id}`, options });
    }

    const updateStock = (data) => {
        const { id } = data;
        const options = { method: 'PUT', body: JSON.stringify(data) }
        return fetchData({ url: `${serviceUrl}/${id}`, options });
    }

    const deleteStock = (id) => {
        const options = { method: 'DELETE' }
        return fetchData({ url: `${serviceUrl}/${id}`, options });
    }

    return { getStocks, getStock, addStock, updateStock, deleteStock, depleteStock };

}

export default StockService;