import { useQuery } from "react-query";
import { MODULE_QUERY_NAMES } from "../../utils/constants";
import StockService from "./stocksService";

const useStocksData = () => {

    const { getStocks } = StockService();

    const queryOptions = { retry: 1, cacheTime: 10000, staleTime: 0, refetchInterval: 10000, refetchIntervalInBackground: true, enabled: true };

    const { isLoading, isFetching, error, data } = useQuery(MODULE_QUERY_NAMES.STOCKS.ALL, getStocks, queryOptions);

    return { isLoading, isFetching, error, data }
}

export default useStocksData