import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router';
import StocksModal from '../stockModal/StocksModal' //Acá

import { newFormValues } from "../stockModal/useStocksModal"; 
import useStockData from '../useStockData';

const StocksAdd = () => {

  const { id } = useParams();
  const [visible, setVisible] = useState(true);
  const [editStock, setEditStock] = useState(newFormValues);

  const validateStock = useStockData(id);

  useEffect(() => {
    validateStock.refetch();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  useEffect(() => {
    if (validateStock.data && !validateStock.data.error) {
      const stockData = {
        id: validateStock.data.stockId,
        name: validateStock.data.name,
        quantity: validateStock.data.quantity,
        price: validateStock.data.price,
        registerDate: validateStock.data.registerDate, 
      };

      setEditStock(stockData)
    }
  }, [validateStock.data]);

  return (
    <>
        <StocksModal
          productFetch = {id ? validateStock : null}
          key={new Date().getTime()}
          visible={visible}
          setVisible={setVisible}
          editValues={editStock}
      ></StocksModal>
    </>
  )
}

export default StocksAdd