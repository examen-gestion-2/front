/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState} from "react";

import DataTable from "react-data-table-component";
import CustomLoader from "../../../shared/components/CustomLoader";

import useStocksData from "../useStocksData";
import useStocksMutations from "../useStockMutation";
import useStockTable from "./useStockTable";
import useAlerts from "../../../hooks/useAlerts";
import { useNavigate } from "react-router";
import TableToolbar from "../../../shared/components/TableToolbar";
import { TABLE_OPTIONS } from "../../../utils/constants";
import StockService from "../stocksService";

const StocksTable = () => {

  const { depleteStock } = StockService();

  const { handleDelete } = useStocksMutations();
  const { isLoading, data } = useStocksData();

  const { promiseAlert, toastAlert } = useAlerts();

  const { formatDate } = useStockTable();

  const nav = useNavigate();

  const [filteredData, setFilteredData] = useState([]);
  const [searchValue, setSearchValue] = useState('');

  useEffect(() => {
    
    setFilteredData(data);
    
  }, [data]);

  const handleDepletion = (id) => {
    promiseAlert(
      "Atención",
      `¿Seguro deseas marcar como agotado?, esta acción es irreversible."`
    ).then((data) => {
      if (data.isConfirmed) {
        depleteStock(id)
        .then(()=>{ toastAlert("Se marcó el stock correctamente")})
        .catch(()=>{ toastAlert("Surgió un error, intentalo nuevamente")})
      }
    });
  }

  const columns = [
    {
      name: "Nombre",
      selector: (row) => row.name,
    },
    {
      name: "Cantidad",
      selector: (row) => row.quantity,
    },
    {
      name: "Precio",
      selector: (row) => '₡ ' + Number(row.price).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") ,
    },
    {
      name: "Fecha de registro",
      selector: (row) => formatDate(row.registerDate),
    },
    {
      name: "Agotamiento",
      cell: (row) => {
        return <span className="text-danger text-center">
        {
          row.depletionDate && formatDate(row.depletionDate)
        }
        {
          !row.depletionDate && <button name="butonDel"type="button" title="Marcar como agotado"
            onClick={()=>{ handleDepletion(row.stockId) }}
            className="btn btn-danger btn-sm mx-1"><i className="ico i__close"/>
            </button>
        }
        </span>
        
      },
    },
    {
      name: "Acciones",
      cell: (row) => {
        return (
          <>
            <button name="butonDel"
              onClick={() => {
                handleDeleteStock(row.stockId, row.name);
              }}
                type="button"
                className="btn btn-danger btn-sm mx-1"
            >
             <i className="ico i__delete"/>
            </button>
            
            <button name="butonEdit"
              onClick={() => {
                handleEditStock(row.stockId);
              }}
                type="button"
                className="btn btn-primary btn-sm"
            >
               <i className="ico i__edit"/>
            </button>
          </>
        );
      },
      ignoreRowClick: true,
      allowOverflow: true,
      button: true,
    },
  ];

  const handleAddClient = () => {
    nav("/stock/add");
  };

  const handleEditStock = (id) => {
    nav(`/stock/edit/${id}`);
  };

  const handleDeleteStock = (id, name) => {
    promiseAlert(
      "Atención",
      `¿Seguro deseas eliminar este producto del inventario, ${name}, esta acción es irreversible."`
    ).then((data) => {
      if (data.isConfirmed) {
        handleDelete(id);
      }
    });
  };


  const handleFilterChange = (event) => {
    setSearchValue(event.target.value.toLowerCase());
  }

  function search(){
    if(searchValue !== ''){
      const filtered = data.filter(item =>item.name.toLowerCase().includes(searchValue) );
      setFilteredData(filtered);
    }else{
      setFilteredData(data);
    }
  }

  useEffect(() => {
    if (!data) return;

    setFilteredData(data.filter(item =>item.name.toLowerCase().includes(searchValue) ));
  }, [data])
  
  const toolBarData = {
    handleAdd: handleAddClient,
    handleFilterChange,
    searchValue,
    search,
  };


  return (
    <>
    
    <div>
    <h1>Inventario</h1>
    <TableToolbar {...toolBarData} />

      <DataTable 
      {...TABLE_OPTIONS}
      columns={columns}
      data={filteredData}
      progressPending={isLoading}
      progressComponent={<CustomLoader />}
      />
    </div>
    </>
  );
  }
export default StocksTable;
 