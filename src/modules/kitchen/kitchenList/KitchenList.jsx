import { Fragment, useState, useEffect } from "react";

import useOrderData from "../../orders/useOrderData";
import Modal from "../../../shared/components/Modal";

import "./kitchenList.scss";
import KitchenModal from "./KitchenModal";

import StateButton from "./StateButton";
import useKitchenList from "./useKitchenList";

import OrdersList from "./OrdersList";
import useKitchenData from "../useKitchenData";

const KitchenList = () => {
  const [visible, setVisible] = useState(false);
  const [selectedOrder, setSelectedOrder] = useState(0);

  const openOrder = (id) => {
    setSelectedOrder(id);
    setVisible(true);
  };

  const orders = useKitchenData().data;

  let { data, refetch } = useOrderData(selectedOrder);

  const { stateLinks } = useKitchenList();

  useEffect(() => {
    refetch();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedOrder]);

  let sections;
  if(orders){
    sections = [
      {orders: orders.filter((order) => order.state === 0), title:"Nuevos", emptyText:"nuevas.", id: "new"},
      {orders: orders.filter((order) => order.state === 1), title:"En proceso", emptyText:"en proceso.", id: "procesing"},
      {orders: orders.filter((order) => order.state === 2), title:"Entregados", emptyText:"entregadas.", id: "sent"},
      {orders: orders.filter((order) => order.state === 3), title:"Cancelados", emptyText:"canceladas.", id: "cancelled"}
  ]

}

  return (
    <div className="orders__container">
      <Modal
        className="order__modal"
        visible={visible}
        handleCancel={() => {
          setVisible(false);
        }}
      >
        <KitchenModal data={data} setVisible={setVisible}/>
      </Modal>

        {sections != null && (
        <div className="row_order">
          <h1 className="">Pedidos</h1>
          <div className="orders__nav-container">
          {stateLinks.map((category, i) => <StateButton key={i} {...category} />)}
          </div>
        </div>
      )}
       

      { orders && (
        <>
          { sections.map((section, i) =>  <OrdersList key={i} openOrder={openOrder} {...section}/>) }
        </>
      )}
    </div>
  );
};

export default KitchenList;
