import React, { useEffect, useState } from 'react'
import { PAY_METHODS } from '../../../utils/constants';

import orderStatus from "../order_status.json"
import useKitchenMutations from '../useKitchenMutations';

const KitchenModal = ({data, setVisible}) => {
  
  const { handleUpdate } = useKitchenMutations();

  const [inter, setInter] = useState(-1)
  const [timer, setTimer] = useState(5);

  const onChangeState = () => {

    if (timer === 5){
      startInterval()
    }else{
      handleUpdate({ id: data.orderId, state: data.state+1 });
      setVisible(false);
    }

  };

  const startInterval = () => {
      
      const interval = setInterval(() => {
        setTimer(timer => timer - 1);
      }, 1000);

      setInter(interval)

      return () => clearInterval(interval);
  }

  useEffect(() => {
    if(timer === 0){
        window.clearInterval(inter); 
        setTimer(5)
        setInter(-1)
    }
  
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [timer])
  
  return ( 
    <>
     {
         data &&
        <div className="order__container-detail">
        <h2>#{ data.orderId}</h2>

        <h4 className="txt3-order date-order">{ data.registerDate}</h4>
        <div className="content-order">
          <p className="txt4-order">{ data.name}</p>
          <p className="txt6-order">{ data.address}</p>
        </div>
        <div className="row row-detail">
          {
            data.products.map((product, index) => (
              <div key={index} className="col content-order-detail">
                <p className="txt4-order">Productos</p>
                <p className="txt3-order">
                  {product.quantity}{" "}
                  <span className="text-muted">{product.name}</span> ₡{" "}
                  {product.price}
                </p>
              </div>
            ))}
          <div className="col content-order-detail">
            <p className="txt4-order">Detalles</p>
            <p className="txt3-order">
              <span className="text-muted">Costo del envio:</span>{" "}
              { data.shippingCost}
            </p>
            <p className="txt3-order">
              <span className="text-muted">Costo del empaque:</span>{" "}
              { data.emballageCost}
            </p>
            <p className="txt3-order">
            <span className="text-muted">Paga con:</span>{" "}
              { PAY_METHODS[data.paymentMethod]}
            </p>
            <p className="txt3-order">
              <span className="text-muted">Total:</span>{" "}
              <span className="text-danger"><>₡ {Number(data.total).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</></span>
            </p>
          </div>
        </div>
        <div className={"p-1 " + orderStatus[data.state].class} >
          <div className="">
            <p className="txt5-order">{orderStatus[data.state].title}</p>
          </div>
        </div>
        {
          orderStatus[data.state].nextStep && 
          <button className={"footer card__footer-order " + orderStatus[data.state+1].class} onClick={onChangeState}>
            <div className="">


              <p className="txt5-order"> 
              {
                inter !== -1 ?
                "Click para confirmar " + timer :
                orderStatus[data.state].nextStep
              }
             
              </p>
            </div>
          </button>
        }
        

      </div>
     }
        </>
  )
}

export default KitchenModal