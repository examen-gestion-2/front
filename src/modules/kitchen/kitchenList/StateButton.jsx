import React from 'react'

const StateButton = ({link, name}) => {
  return (
    <button className="btn btn-category combo-color-tex col-lg-1"><a className="category-link" href={link}>{name}</a></button>
  )
}

export default StateButton