const useKitchenList = () => {

    const stateLinks = [
        {link: "#procesing", name:"Pedidos en proceso"},
        {link: "#new", name:"Pedidos nuevos"},
        {link: "#sent", name:"Pedidos entregados"},
        {link: "#cancelled", name:"Pedidos cancelados"},
    ]


  return {  stateLinks }
}

export default useKitchenList