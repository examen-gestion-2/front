import useMutations from "../../hooks/useMutations";
import useAlerts from "../../hooks/useAlerts";
import KitchenService from "./kitchenService";

import { MODULE_QUERY_NAMES } from "../../utils/constants";

const useKitchenMutations = () => {
    const { updateOrder } = KitchenService();
    const {toastAlert} = useAlerts();

    const onSuccessUpdate = () => {
        toastAlert("Se actualizó exitosamente el estado", "success");
    }
   
    const onErrorUpdate = () => toastAlert("No se pudo actualizar el estado", "error");
   
    // Base mutations
    const { handleUpdate } = useMutations(
        {
            moduleName:MODULE_QUERY_NAMES.ORDERS.ALL, 
          
            updateFunction:updateOrder,
            onSuccessUpdate,
            onErrorUpdate,
           
        });

    return { handleUpdate }
}

export default useKitchenMutations