import useFetch from "../../hooks/useFetch"

import { URL } from "../../utils/constants";

const serviceUrl = `${URL}/order`

const KitchenService = () => {
    const { fetchData } = useFetch();

    const updateOrder = (data) => {

        const { id } = data;
        const options = { method: 'POST', body: JSON.stringify(data) }
        return fetchData({ url: `${serviceUrl}/state_change/${id}`, options });
    }

    const getToday = () => {
        return fetchData({ url: `${serviceUrl}/today` })
    }

    return { updateOrder, getToday };

}

export default KitchenService;