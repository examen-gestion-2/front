import { useMutation, useQueryClient } from "react-query";

const useMutations = (
    {
        moduleName,
        addFunction,
        deleteFunction,
        updateFunction,
        onSuccessAdd = () => { },
        onSuccessDelete = () => { },
        onSuccessUpdate = () => { },
        onErrorAdd = () => { },
        onErrorDelete = () => { },
        onErrorUpdate = () => { },
    }

) => {
    const queryClient = useQueryClient();

    const successUpdate = () => {
        onSuccessUpdate();
        revalidateQuery();
    }

    const errorUpdate = () => {
        onErrorUpdate();
    }

    const successDelete = () => {
        onSuccessDelete();
        revalidateQuery();
    }

    const errorDelete = () => {
        onErrorDelete();
    }

    const successAdd = () => {
        onSuccessAdd();
        revalidateQuery();
    }

    const errorAdd = () => {
        onErrorAdd();
    }

    // Invalidate data and refetch
    const revalidateQuery = () => {
        queryClient.invalidateQueries(moduleName);
    }

    // Mutation when data is added
    const addMutation = useMutation(addFunction, {
        onSuccess: successAdd,
        onError: errorAdd
    });

    // Mutation when data is deleted
    const deleteMutation = useMutation(deleteFunction, {
        onSuccess: successDelete,
        onError: errorDelete
    });

    // Mutation when data is updated
    const updateMutation = useMutation(updateFunction, {
        onSuccess: successUpdate,
        onError: errorUpdate
    });

    const handleAdd = (userData) => {
        addMutation.mutate(userData);
    };

    const handleDelete = (userId) => {
        deleteMutation.mutate(userId);
    };

    const handleUpdate = (userId) => {
        updateMutation.mutate(userId);
    };

    return { handleAdd, handleDelete, handleUpdate }
}

export default useMutations