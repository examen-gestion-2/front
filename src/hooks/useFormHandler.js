import { useState } from "react"

const convertToState = (init) => {
    const stateObject = {}
    const keys = Object.keys(init)

    for (const key of keys) {
        stateObject[key] = {
            [`state`]: init[key],
            [`isValid`]: false,
            [`message`]: "",
        }
    }

    return stateObject;
}

const useFormHandler = (init) => {
    const [state, setState] = useState(convertToState(init))

    const updateState = (value, valueName) => {
        if (!(valueName in state)) return
        setState({ ...state, [valueName]: { ...state[valueName], [`state`]: value } })
    }

    const updateValidation = (value, valueName) => {
        if (!(valueName in state)) return
        setState({ ...state, [valueName]: { ...state[valueName], [`isValid`]: value } })
    }

    const updateMessage = (value, valueName) => {
        if (!(valueName in state)) return

        if (value) {
            setState({ ...state, [valueName]: { ...state[valueName], [`message`]: value, [`isValid`]: true } })
        } else {
            setState({ ...state, [valueName]: { ...state[valueName], [`message`]: value, [`isValid`]: false } })
        }
    }

    const getJson = () => {
        const keys = Object.keys(state);
        const json = {}
        for (const key of keys) {
            json[key] = state[key]['state']
        }
       return json;

    }
    const getState = (valueName) => {
        return state[valueName];
    }

    return { updateState, updateValidation, updateMessage, getState, getJson, state }
}

export default useFormHandler