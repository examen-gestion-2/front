/* eslint-disable react-hooks/exhaustive-deps */
import { useCallback } from "react";

const useImageSystem = () => {
  const convertBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve((reader ).result);
      reader.onerror = (err) => reject(err);
    });
  };

  const downscaleImage = useCallback(async (file) => {
    const dataUrl = await convertBase64(file);
    return rescaleImage(dataUrl, 1024);
  }, []);

  const thumbnailImage = useCallback(async (file) => {
    return rescaleImage(file, 256);
  }, []);

  const rescaleImage = useCallback(async (dataUrl, size) => {
    
    // If the size is less than 0.5mb will return without converting the file
    const newWidth = size;
    const image = new Image();
    const canvas = document.createElement("canvas");
    const ctx = canvas.getContext("2d");

    image.src = dataUrl;
    await image.decode();

    const newHeight = Math.floor((image.height / image.width) * newWidth);

    canvas.width = newWidth;
    canvas.height = newHeight;

    ctx.drawImage(image, 0, 0, newWidth, newHeight);

    const newDataUrl = canvas.toDataURL("image/webp", 0.6);

    return newDataUrl;
  }, []);


  return { downscaleImage, thumbnailImage };
};

export default useImageSystem;