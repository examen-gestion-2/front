
const useFetch = () => {
    const fetchData = ({url, options}) => fetch(url, options).then(res =>res.json());
    return { fetchData }
}

export default useFetch;