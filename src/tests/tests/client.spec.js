
/*
 * Dependency Modules
 */

const { Builder, Browser, By } = require('selenium-webdriver');
const { URLS } = require('../utils/constants');
const assert = require("assert").strict;
const { randomUUID } = require('crypto');
require("geckodriver");

const timeoutTimer = 2500

describe('02. Happy Sushi Client Test Cases', function () {
    this.timeout(30000);
    let vars = {};
    let driver;

    const ERRORS = {
        NAME: {
            EMPTY: "El nombre es requerido",
            SHORT: "Se requieren al menos 3 carácteres",
            LONG: "El máximo es de 25 carácteres"
        },
        PHONE: {
            EMPTY: "El Teléfono es requerido",
            SHORT: "Se requieren al menos 8 carácteres",
            LONG: "El máximo es de 8 carácteres"
        },
        ADDRESS: {
            EMPTY: "La dirección es requerida",
            SHORT: "Se requieren al menos 5 carácteres",
            LONG: "El máximo es de 75 carácteres"
        }
    }

    beforeEach(async function () {
        driver = new Builder().forBrowser(Browser.FIREFOX).build()
        await driver.get(URLS.LOGIN);

        // Find the access admin icon
        vars["adminIcon"] = await driver.findElements(By.xpath("//img[@class=\"admin_icon\"]"))
        // If the access admin icon not exists log out
        if (!vars["adminIcon"]) {
            await driver.findElement(By.css("a:nth-child(12) .MuiSvgIcon-root")).click();
            await driver.get(URLS.LOGIN);
        }

        // Load the input fields
        const userNameElement = await driver.findElement(By.id(":r0:"));
        const passwordElement = await driver.findElement(By.id(":r1:"));

        // Login with admin account
        await userNameElement.sendKeys("Admin");
        await passwordElement.sendKeys("123123");

        // Click the button
        await driver.findElement(By.css(".btn")).click();
        await new Promise(resolve => setTimeout(resolve, timeoutTimer));
        await driver.get(URLS.CLIENTS);
        await new Promise(resolve => setTimeout(resolve, timeoutTimer));
    })

    afterEach(async function () {
        vars = {}
        await driver.quit();
    })

    /**
     * 02. Clients test case 🗒️
     * 
     * Description: 
     *      Validates that clients table can be accessed as admin
     * 
     * Success conditions:
     *      ✔️ Show text "Admin Happy Sushi" as title of the page
     *      ✔️ Show text "Clientes" as title of the table
     *      ✔️ Show the table 
     */

    it('01. Assert load client table as admin 🚀', async function () {

        // Test if the title of the table is "Clientes"
        assert(await driver.findElement(By.css("h1")).getText() === "Clientes");

        // Test if the table exists
        assert(await driver.findElements(By.css(".sc-dmctIk")))

    })

    /**
     * 02. Clients test case 🗒️
     * 
     * Description: 
     *      Validates that on wrong inputs UI show error labels
     * 
     * Success conditions:
     *      ✔️ Show text "Se requieren al menos 3 carácteres" on too short name input
     *      ✔️ Show text "El nombre es requerido" on empty name input
     *      ✔️ Show text "El máximo es de 25 carácteres" on too long name input
     */

    it('02. Assert wrong text on client inputs 🚀', async function () {

        // Click on add
        await driver.findElement(By.css(".btn-add")).click();

        // Load the input fields
        vars["name"] = await driver.findElement(By.id(":r4:"));
        
        // Test too short name input msg
        vars["name"].sendKeys("A");
        assert(await driver.findElement(By.css(".col > .form-floating:nth-child(1) > .input-error")).getText() === ERRORS.NAME.SHORT);

        // Test empty name input msg
        vars["name"].clear();
        assert(await driver.findElement(By.css(".col > .form-floating:nth-child(1) > .input-error")).getText() === ERRORS.NAME.EMPTY);

        // Test too long input msg
        vars["name"].sendKeys("Some long long long long long long name");
        assert(await driver.findElement(By.css(".col > .form-floating:nth-child(1) > .input-error")).getText() === ERRORS.NAME.LONG);
    })

    /**
     * 02. Clients test case 🗒️
     * 
     * Description: 
     *      Validates that on add show toast and the last client is added
     * 
     * Success conditions:
     *      ✔️ Show text "Se agregó exitosamente" on toast
     *      ✔️ Show last client name "Adam + UUID"
     */

    it('03. Assert toast on add client 🚀', async function () {

        // Generate a random UUID for this test
        const UUID = randomUUID().substring(0, 5);

        // Click on add
        await driver.findElement(By.css(".btn-add")).click();

        // Load the input fields
        vars["name"] = await driver.findElement(By.id(":r4:"));
        vars["phone"] = await driver.findElement(By.id(":r5:"));
        vars["date"] = await driver.findElement(By.id(":r6:"));
        vars["address"] = await driver.findElement(By.id(":r7:"));

        // Add correct data 
        vars["name"].sendKeys(`Adam ${UUID}`);
        vars["phone"].sendKeys("84568468");
        vars["date"].sendKeys("2023-01-27");
        vars["address"].sendKeys("Some place");

        // Click the add button
        await driver.findElement(By.css(".btn-primary")).click();
        await new Promise(resolve => setTimeout(resolve, timeoutTimer));

        // Test the toast text is "Se agregó exitosamente"
        assert(await driver.findElement(By.id("swal2-title")).getText() === "Se agregó exitosamente");

        // Test that the last user added name is "Adam + UUID"
        assert(await driver.findElement(By.css("#row-0 > #cell-1-undefined > div")).getText() === `Adam ${UUID}`);

    })

   

    /**
    * 02. Clients test case 🗒️
    * 
    * Description: 
    *      Validates that search is filtering
    * 
    * Success conditions:
    *      ✔️ Shows filtered client with substring "adam"
    */

    it('04. Assert search filter 🚀', async function () {

        // Load the text field
        vars["search"] = await driver.findElement(By.name("busqueda"));

        // Set the search param
        vars["search"].sendKeys("adam");

        // Click on search button
        await driver.findElement(By.css(".i__search")).click();

        await new Promise(resolve => setTimeout(resolve, timeoutTimer));

        // Load last client
        vars["client"] = await driver.findElement(By.css("#row-0 > #cell-1-undefined > div")).getText();

        // Test that the last client name contains the search param
        assert(vars["client"].toLowerCase().includes("adam"));
    })

    /**
    * 02. Clients test case 🗒️
    * 
    * Description: 
    *      Validates that shows toast after delete
    * 
    * Success conditions:
    *      ✔️ Show toast on delete
    *      ✔️ Show text "Se eliminó exitosamente" on toast
    */

    it('05. Assert alert after delete 🚀', async function () {

        // Click the delete btn
        await driver.findElement(By.css("#row-0 .btn-danger > .ico")).click()
        await new Promise(resolve => setTimeout(resolve, timeoutTimer));

        // Click the confirm modal btn   
        await driver.findElement(By.css(".swal2-confirm")).click()
        await new Promise(resolve => setTimeout(resolve, timeoutTimer));

        // Test that the toast is shown
        assert(await driver.findElements(By.css(".swal2-popup")))

        // Test that the toast contains the text "Se eliminó exitosamente"
        assert(await driver.findElement(By.id("swal2-title")).getText() === "Se eliminó exitosamente")

    })

})