
/*
 * Dependency Modules
 */

const { Builder, Browser, By } = require('selenium-webdriver');
const { URLS } = require('../utils/constants');
const assert = require("assert").strict;
require("geckodriver");

const timeoutTimer = 2500

describe('01. Happy Sushi Login Test Cases', function () {
    this.timeout(30000);
    let driver;
    let vars = {};

    const ERRORS = {
        USERNAME: {
            EMPTY: "El nombre de usuario es requerido",
            SHORT: "Se requieren al menos 5 carácteres",
            LONG: "El máximo es de 25 carácteres"
        },
        PASSWORD: {
            EMPTY: "La contraseña es requerida",
            SHORT: "Se requieren al menos 5 carácteres",
            LONG: "El máximo es de 75 carácteres"
        }
    }

    beforeEach(async function () {
        driver = new Builder().forBrowser(Browser.FIREFOX).build()
        await driver.get(URLS.LOGIN);

        // Find the access admin icon
        vars["admin_icon"] = await driver.findElements(By.xpath(`//img[@class="admin_icon"]`));

        // If the access admin icon not exists log out
        if (!vars["admin_icon"]) {
            await driver.findElement(By.css("a:nth-child(12) .MuiSvgIcon-root")).click();
            await driver.get(URLS.LOGIN);
        }
    })

    afterEach(async function () {
        vars = {}
        await driver.quit();
    })

    /**
     * 01. Login test case 🗒️
     * 
     * Description: 
     *      Validates that on wrong credentials shows an error alert
     * 
     * Success conditions:
     *      ✔️ Show text "Inicio de sesión inválido." after wrong login
     */

    it('01. Assert alert on wrong credentials 🚀', async function () {

        // Load the input fields
        const userNameElement = await driver.findElement(By.id(":r0:"));
        const passwordElement = await driver.findElement(By.id(":r1:"));

        // Add wrong data 
        userNameElement.sendKeys("Other");
        passwordElement.sendKeys("nones");

        // Click the button
        await driver.findElement(By.css(".btn")).click();
        await new Promise(resolve => setTimeout(resolve, timeoutTimer));

        // Test the text msg
        assert(await driver.findElement(By.id("swal2-title")).getText() === "Inicio de sesión inválido.");

    })

    /**
     * 02. Login test case 🗒️
     * 
     * Description: 
     *      Validates that on wrong inputs UI show error labels
     * 
     * Success conditions:
     *      ✔️ Show text "Se requieren al menos 5 carácteres" on too short username input
     *      ✔️ Show text "El nombre de usuario es requerido" on empty username input
     *      ✔️ Show text "El máximo es de 25 carácteres" on too long username input
     */

    it('02. Assert wrong text on login inputs 🚀', async function () {

        // Load the input fields
        vars["userName"] = await driver.findElement(By.id(":r0:"));
        vars["password"] = await driver.findElement(By.id(":r1:"));

        // Test too short username input msg
        vars["userName"].sendKeys("A")
        assert(await driver.findElement(By.css(".form-floating:nth-child(1) > .input-error")).getText() === ERRORS.USERNAME.SHORT)


        // Test empty username input msg
        vars["userName"].clear()
        assert(await driver.findElement(By.css(".form-floating:nth-child(1) > .input-error")).getText() === ERRORS.USERNAME.EMPTY)


        // Test too long username input msg
        vars["userName"].sendKeys("Some extra long user name here")
        assert(await driver.findElement(By.css(".form-floating:nth-child(1) > .input-error")).getText() === ERRORS.USERNAME.LONG)


    })

    /**
     * 03. Login test case 🗒️
     * 
     * Description: 
     *      Validates a successful login
     * 
     * Success conditions:
     *      ✔️ The sidebar exists
     *      ✔️ The title text is "Admin Happy Sushi"
     *      ✔️ The admin access button disappears"
     */

    it('03. Assert successful login 🚀', async function () {

        // Load the input fields
        const userNameElement = await driver.findElement(By.id(":r0:"));
        const passwordElement = await driver.findElement(By.id(":r1:"));

        // Add correct data 
        userNameElement.sendKeys("Admin");
        passwordElement.sendKeys("123123");

        // Click the button
        await driver.findElement(By.css(".btn")).click();

        await new Promise(resolve => setTimeout(resolve, timeoutTimer));

        // Test if the sidebar exists
        const elevation = await driver.findElements(By.css(".MuiPaper-elevation0"));
        assert(elevation.length);

        // Test that the access admin icon disappears
        const adminIcon = await driver.findElements(By.css(".admin_icon"));
        assert(!adminIcon.length)

    })

})