/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect } from "react";

import { Outlet, useNavigate } from "react-router-dom";

import { useGlobalUser } from "../contexts/globalContext";

const AdminGuard = ({ rol }) => {

  const { isLogged, getRol } = useGlobalUser();
  const navigate = useNavigate();

  useEffect(() => {
    if (!isLogged() || getRol() !== rol) {
      navigate("/un-authorized");
    }
  }, [getRol, isLogged, navigate]);

  return <Outlet/>;
};

export default AdminGuard;
