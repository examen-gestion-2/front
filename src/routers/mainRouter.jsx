import { Routes, Route, BrowserRouter as Router } from "react-router-dom";
import { Fragment } from "react";

import Error404 from "../modules/general/error404/Error404";
import User from "../modules/users/pages/index/Users";

import Icons from "../modules/general/icons/Icons";

import Template from "../shared/template/Template";
import Login from "../modules/general/login/Login";
import Guard from "../guards/Guard";
import { ROLS } from "../utils/constants";
import ErrorUnauthorized from "../modules/general/errorUnauthorized/ErrorUnauthorized";
import ProductsTable from "../modules/products/productsTable/ProductsTable";
import ProductsAdd from "../modules/products/productsAdd/ProductsAdd";
import ProductsMenu from "../modules/products/productsMenu/ProductsMenu";
import ClientsTable from "../modules/clients/clientsTable/ClientsTable";
import ClientsAdd from "../modules/clients/clientsAdd/ClientsAdd";
import AdminList from "../modules/orders/AdminList/AdminList";
import KitchenList from "../modules/kitchen/kitchenList/KitchenList";

import StocksAdd from "../modules/stock/stockAdd/StocksAdd";
import StocksTable from "../modules/stock/stockTable/StocksTable";
import OrdersAdd from "../modules/orders/ordersAdd/OrdersAdd";

const AuthRouter = () => {
  /**
   * This mehod contains the public routes of the app
   * @returns Public routes of the app
   */
  const PublicRoutes = () => (
    <Fragment>
      <Route path="/" element={<ProductsMenu />}></Route>
      <Route path="/menu" element={<ProductsMenu />}></Route>
    </Fragment>
  );

  /**
   * This mehod contains the private routes of the app
   * @returns Private routes of the app
   */
  const AdminRoutes = () => (
      <Route path="/" element={<Guard rol={ROLS.ADMIN} />}>
        <Route path="/users" element={<User />} />
        <Route path="/products" element={<ProductsTable/>} />
        <Route path="/products/add" element={<ProductsAdd/>} />
        <Route path="/products/edit/:id" element={<ProductsAdd/>} />
        <Route path="/orders" element={<AdminList/>} />
        <Route path="/clients" element={<ClientsTable/>} />
        <Route path="/clients/add" element={<ClientsAdd/>} />
        <Route path="/clients/edit/:id" element={<ClientsAdd/>} />
        <Route path="/billing" element={<>Facturacion</>} />
        <Route path="/reports" element={<>Reportes</>} />
        <Route path="/stock"element={<StocksTable/>} />
        <Route path="/stock/add" element={<StocksAdd/>} />
        <Route path="/stock/edit/:id" element={<StocksAdd/>} />
        <Route path="/orders/add" element={<OrdersAdd/>} /> 
      </Route>
  );

  /**
   * This mehod contains the private routes of the app
   * @returns Private routes of the app
   */
    const KitchenRoutes = () => (
      <Route path="/" element={<Guard rol={ROLS.KITCHEN}/>}>
         <Route path="/kitchen" element={<KitchenList />} />
      </Route>
    );
  
  /**
   * This mehod contains the login routes of the app
   * @returns Login routes of the app
   */
  const LoginRoutes = () => (
    <Fragment>
      <Route path="/login" element={<Login />}></Route>
    </Fragment>
  );

  // TODO: Remove on production. 
  /**
   * This mehod contains dev routes of the app
   * @returns Develop routes of the app
   */
  const DevRoutes = () => (
    <Fragment>
      <Route path="/icons" element={<Icons />}></Route>
    </Fragment>
  );

  return (
    <Router>
      <Routes>

      <Route path="/" element={<Template />}>

        {PublicRoutes()}
        {KitchenRoutes()}
        {AdminRoutes()}
        {LoginRoutes()}
        {DevRoutes()}

        <Route path="/un-authorized" element={<ErrorUnauthorized/>} />
        <Route path="*" element={<Error404 />} />

        </Route>

      </Routes>
    </Router>
  );
};

export default AuthRouter;