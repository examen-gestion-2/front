import { QueryClient, QueryClientProvider } from 'react-query'
import { ReactQueryDevtools } from 'react-query/devtools'

import './app.scss';

import { GlobalProvider } from './contexts/globalContext';

import AuthRouter from './routers/mainRouter';

const queryClient = new QueryClient()

function App() {

  return (
    <QueryClientProvider client={queryClient}>
      <GlobalProvider>
        <AuthRouter></AuthRouter>
        <ReactQueryDevtools initialIsOpen={false}></ReactQueryDevtools>
      </GlobalProvider>
    </QueryClientProvider>
  );
}

export default App;
